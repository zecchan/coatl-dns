﻿namespace CoatlDNS
{
    partial class FConfigCoatlDNS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FConfigCoatlDNS));
            this.cxMinTray = new System.Windows.Forms.CheckBox();
            this.cxAutoStart = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.cxCache = new System.Windows.Forms.CheckBox();
            this.nudCache = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbArgs = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbWriteable = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbReadable = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbWorkDir = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ttReadable = new System.Windows.Forms.ToolTip(this.components);
            this.ttWriteable = new System.Windows.Forms.ToolTip(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txServerName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudCache)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cxMinTray
            // 
            this.cxMinTray.AutoSize = true;
            this.cxMinTray.Location = new System.Drawing.Point(27, 56);
            this.cxMinTray.Name = "cxMinTray";
            this.cxMinTray.Size = new System.Drawing.Size(133, 17);
            this.cxMinTray.TabIndex = 1;
            this.cxMinTray.Text = "Minimize to system tray";
            this.cxMinTray.UseVisualStyleBackColor = true;
            // 
            // cxAutoStart
            // 
            this.cxAutoStart.AutoSize = true;
            this.cxAutoStart.Location = new System.Drawing.Point(27, 79);
            this.cxAutoStart.Name = "cxAutoStart";
            this.cxAutoStart.Size = new System.Drawing.Size(165, 17);
            this.cxAutoStart.TabIndex = 2;
            this.cxAutoStart.Text = "Auto start on windows startup";
            this.cxAutoStart.UseVisualStyleBackColor = true;
            this.cxAutoStart.CheckedChanged += new System.EventHandler(this.cxAutoStart_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(155, 261);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(236, 261);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cxCache
            // 
            this.cxCache.AutoSize = true;
            this.cxCache.Location = new System.Drawing.Point(27, 102);
            this.cxCache.Name = "cxCache";
            this.cxCache.Size = new System.Drawing.Size(173, 17);
            this.cxCache.TabIndex = 3;
            this.cxCache.Text = "Cache DNS resolver messages";
            this.cxCache.UseVisualStyleBackColor = true;
            this.cxCache.CheckedChanged += new System.EventHandler(this.cxCache_CheckedChanged);
            // 
            // nudCache
            // 
            this.nudCache.Location = new System.Drawing.Point(139, 125);
            this.nudCache.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudCache.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCache.Name = "nudCache";
            this.nudCache.Size = new System.Drawing.Size(61, 20);
            this.nudCache.TabIndex = 4;
            this.nudCache.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Maximum cache";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbArgs);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lbWriteable);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lbReadable);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lbWorkDir);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(27, 148);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(284, 107);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Information";
            // 
            // lbArgs
            // 
            this.lbArgs.AutoSize = true;
            this.lbArgs.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbArgs.Location = new System.Drawing.Point(12, 84);
            this.lbArgs.Name = "lbArgs";
            this.lbArgs.Size = new System.Drawing.Size(35, 13);
            this.lbArgs.TabIndex = 7;
            this.lbArgs.Text = "label3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(6, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "CMD Arguments";
            // 
            // lbWriteable
            // 
            this.lbWriteable.AutoSize = true;
            this.lbWriteable.Location = new System.Drawing.Point(199, 51);
            this.lbWriteable.Name = "lbWriteable";
            this.lbWriteable.Size = new System.Drawing.Size(35, 13);
            this.lbWriteable.TabIndex = 5;
            this.lbWriteable.Text = "label3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(135, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Writeable?";
            this.ttWriteable.SetToolTip(this.label5, "This will determine whether the configuration files can be \r\nwritten or not. If i" +
        "t cannot be written in current directory, \r\nit will be written to current user\'s" +
        " LocalApplicationData.");
            // 
            // lbReadable
            // 
            this.lbReadable.AutoSize = true;
            this.lbReadable.Location = new System.Drawing.Point(71, 51);
            this.lbReadable.Name = "lbReadable";
            this.lbReadable.Size = new System.Drawing.Size(35, 13);
            this.lbReadable.TabIndex = 3;
            this.lbReadable.Text = "label3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(6, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Readable?";
            this.ttReadable.SetToolTip(this.label3, "This will determine whether the configuration files can be read or not.");
            // 
            // lbWorkDir
            // 
            this.lbWorkDir.AutoSize = true;
            this.lbWorkDir.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbWorkDir.Location = new System.Drawing.Point(12, 32);
            this.lbWorkDir.Name = "lbWorkDir";
            this.lbWorkDir.Size = new System.Drawing.Size(35, 13);
            this.lbWorkDir.TabIndex = 1;
            this.lbWorkDir.Text = "label3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Current Directory";
            // 
            // ttReadable
            // 
            this.ttReadable.IsBalloon = true;
            this.ttReadable.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.ttReadable.ToolTipTitle = "Readable Directory";
            // 
            // ttWriteable
            // 
            this.ttWriteable.AutoPopDelay = 8000;
            this.ttWriteable.InitialDelay = 500;
            this.ttWriteable.IsBalloon = true;
            this.ttWriteable.ReshowDelay = 100;
            this.ttWriteable.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.ttWriteable.ToolTipTitle = "Writeable Directory";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(272, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Server Name (leave it blank to use your computer name)";
            // 
            // txServerName
            // 
            this.txServerName.Location = new System.Drawing.Point(27, 30);
            this.txServerName.Name = "txServerName";
            this.txServerName.Size = new System.Drawing.Size(284, 20);
            this.txServerName.TabIndex = 0;
            // 
            // FConfigCoatlDNS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 296);
            this.Controls.Add(this.txServerName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudCache);
            this.Controls.Add(this.cxCache);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cxAutoStart);
            this.Controls.Add(this.cxMinTray);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FConfigCoatlDNS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Coatl DNS Configuration";
            this.Load += new System.EventHandler(this.FConfigCoatlDNS_Load);
            this.Shown += new System.EventHandler(this.FConfigCoatlDNS_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.nudCache)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cxMinTray;
        private System.Windows.Forms.CheckBox cxAutoStart;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox cxCache;
        private System.Windows.Forms.NumericUpDown nudCache;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbWorkDir;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbReadable;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbWriteable;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolTip ttReadable;
        private System.Windows.Forms.ToolTip ttWriteable;
        private System.Windows.Forms.Label lbArgs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txServerName;
    }
}