﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Windows.Forms;

namespace CoatlDNS
{
    public partial class FSetupInterface : Form
    {
        NetworkInterface[] networkInterfaces;

        public FSetupInterface()
        {
            InitializeComponent();

            WinInterop.AddShieldToButton(button2);
            WinInterop.AddShieldToButton(button3);

            RefreshNIC();
        }

        private void RefreshNIC(string autoSelect = null)
        {
            ddNetInterface.Items.Clear();
            try
            {
                networkInterfaces = NetworkInterface.GetAllNetworkInterfaces()
                    .Where(ni =>
                    ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet
                    || ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211
                    ).ToArray();
                foreach (var ni in networkInterfaces)
                {
                    ddNetInterface.Items.Add(ni.Name + " - " + ni.Description);
                }
                try
                {
                    if (ddNetInterface.Items.Count > 0)
                    {
                        if (autoSelect != null)
                        {
                            var ni = networkInterfaces.Where(n => n.Id == autoSelect).FirstOrDefault();
                            if (ni != null)
                            {
                                ddNetInterface.SelectedIndex = networkInterfaces.ToList().IndexOf(ni);
                            }
                        }
                        else
                        {
                            NetworkInterface def = null;
                            int minIdx = int.MaxValue;
                            foreach (var ni in networkInterfaces)
                            {
                                try
                                {
                                    if (ni.OperationalStatus != OperationalStatus.Up) continue;
                                    if (ni.GetIPProperties()?.GetIPv4Properties() != null)
                                    {
                                        var ip4p = ni.GetIPProperties()?.GetIPv4Properties();
                                        if (ip4p.Index < minIdx)
                                        {
                                            def = ni;
                                            minIdx = ip4p.Index;
                                        }
                                    }
                                }
                                catch { }
                            }
                            if (def != null)
                                ddNetInterface.SelectedIndex = networkInterfaces.ToList().IndexOf(def);
                        }
                        if (ddNetInterface.SelectedIndex < 0 && ddNetInterface.Items.Count > 0) ddNetInterface.SelectedIndex = 0;
                    }
                }
                catch { }
            }
            catch { 
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ddNetInterface_SelectedIndexChanged(object sender, EventArgs e)
        {
            button2.Enabled = false;
            button3.Enabled = false;
            if (ddNetInterface.SelectedIndex < 0) return;

            try
            {
                var ni = networkInterfaces[ddNetInterface.SelectedIndex];

                lbNICName.Text = ni.Name;
                lbNICDesc.Text = ni.Description;
                lbNICStatus.Text = ni.OperationalStatus.ToString();
                lbNICStatus.ForeColor = ni.OperationalStatus == OperationalStatus.Up || ni.OperationalStatus == OperationalStatus.Testing ? System.Drawing.Color.Green : System.Drawing.Color.Red;
                lbNICType.Text =
                    ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet ? "Ethernet" :
                    ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 ? "Wi-Fi" :
                    ni.NetworkInterfaceType.ToString();
                var ipprop = ni.GetIPProperties();
                lbNICIPAddr.Text = "N/A";
                lbNICIPPrio.Text = ni?.GetIPProperties()?.GetIPv4Properties().Index.ToString() ?? "N/A";
                lbNICIPPrioV6.Text = ni?.GetIPProperties()?.GetIPv6Properties().Index.ToString() ?? "N/A";
                if (ipprop != null)
                {
                    string ipv4 = null;
                    string ipv6 = null;
                    foreach (var ua in ipprop.UnicastAddresses)
                    {
                        if (ua.Address.AddressFamily == AddressFamily.InterNetwork)
                            ipv4 = ua.Address.ToString();
                        if (ua.Address.AddressFamily == AddressFamily.InterNetworkV6)
                            ipv6 = ua.Address.ToString();
                    }

                    lbNICIPAddr.Text = ipv4 ?? "N/A";
                    lbNICIPAddrV6.Text = ipv6 ?? "N/A";
                }
                lbNICGateway.Text = ipprop.GatewayAddresses.Where(gw => gw.Address.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault()?.Address?.ToString() ?? "N/A";
                lbNICGatewayV6.Text = ipprop.GatewayAddresses.Where(gw => gw.Address.AddressFamily == AddressFamily.InterNetworkV6).FirstOrDefault()?.Address?.ToString() ?? "N/A";
                lbNICDNS.Text = string.Join("; ", ipprop.DnsAddresses.Select(dns => dns.ToString()));
                button2.Enabled = !ipprop.DnsAddresses.Select(dns => dns.ToString()).ToList().Contains("127.0.0.1");
                button3.Enabled = ipprop.DnsAddresses.Select(dns => dns.ToString()).ToList().Contains("127.0.0.1");
            }
            catch {
                MessageBox.Show("Failed to load NIC Information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (ddNetInterface.SelectedIndex < 0) return;
            var ni = networkInterfaces[ddNetInterface.SelectedIndex];

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.UseShellExecute = true;
            startInfo.WorkingDirectory = Environment.CurrentDirectory;
            startInfo.FileName = Application.ExecutablePath;
            startInfo.Verb = "runas";
            startInfo.Arguments = "-setdns " + ni.Id + " \"" + ni.Name + "\"";
            try
            {
                Process p = Process.Start(startInfo);
                p.WaitForExit();
                if (p.ExitCode != 0)
                    throw new Exception();
                RefreshNIC(ni.Id);
            }
            catch
            {
                MessageBox.Show("Failed to setup Coatl DNS", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (ddNetInterface.SelectedIndex < 0) return;
            var ni = networkInterfaces[ddNetInterface.SelectedIndex];

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.UseShellExecute = true;
            startInfo.WorkingDirectory = Environment.CurrentDirectory;
            startInfo.FileName = Application.ExecutablePath;
            startInfo.Verb = "runas";
            startInfo.Arguments = "-clrdns " + ni.Id + " \"" + ni.Name + "\"";
            try
            {
                Process p = Process.Start(startInfo);
                p.WaitForExit();
                if (p.ExitCode != 0)
                    throw new Exception();
                RefreshNIC(ni.Id);
            }
            catch
            {
                MessageBox.Show("Failed to setup Coatl DNS", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
