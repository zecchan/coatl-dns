﻿namespace CoatlDNS
{
    partial class FConfigCustomDomainMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FConfigCustomDomainMapping));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lbDomain = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txAddDomain = new System.Windows.Forms.TextBox();
            this.btnAddDomain = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txEntries = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.nudTTL = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTTL)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnAddDomain);
            this.groupBox1.Controls.Add(this.txAddDomain);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lbDomain);
            this.groupBox1.Location = new System.Drawing.Point(12, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 429);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Domain Names";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txEntries);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(218, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(326, 400);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "IP Address Map";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(289, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "You can map specific domains to custom IP Addresses here";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(469, 436);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(388, 436);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbDomain
            // 
            this.lbDomain.FormattingEnabled = true;
            this.lbDomain.Location = new System.Drawing.Point(6, 19);
            this.lbDomain.Name = "lbDomain";
            this.lbDomain.Size = new System.Drawing.Size(188, 303);
            this.lbDomain.TabIndex = 0;
            this.lbDomain.SelectedIndexChanged += new System.EventHandler(this.lbDomain_SelectedIndexChanged);
            this.lbDomain.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbDomain_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(256, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Enter mapped IP Addresses, one IP Address per line:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 331);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Add a domain name:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txAddDomain
            // 
            this.txAddDomain.Location = new System.Drawing.Point(10, 348);
            this.txAddDomain.Name = "txAddDomain";
            this.txAddDomain.Size = new System.Drawing.Size(145, 20);
            this.txAddDomain.TabIndex = 2;
            this.txAddDomain.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txAddDomain_KeyPress);
            // 
            // btnAddDomain
            // 
            this.btnAddDomain.Location = new System.Drawing.Point(161, 346);
            this.btnAddDomain.Name = "btnAddDomain";
            this.btnAddDomain.Size = new System.Drawing.Size(33, 23);
            this.btnAddDomain.TabIndex = 3;
            this.btnAddDomain.Text = "+";
            this.btnAddDomain.UseVisualStyleBackColor = true;
            this.btnAddDomain.Click += new System.EventHandler(this.btnAddDomain_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 377);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 39);
            this.label4.TabIndex = 4;
            this.label4.Text = "To delete a domain,\r\nselect the domain name then press \r\nDelete on your keyboard";
            // 
            // txEntries
            // 
            this.txEntries.Location = new System.Drawing.Point(19, 41);
            this.txEntries.Multiline = true;
            this.txEntries.Name = "txEntries";
            this.txEntries.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txEntries.Size = new System.Drawing.Size(290, 342);
            this.txEntries.TabIndex = 1;
            this.txEntries.WordWrap = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(218, 441);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "DNS Record TTL";
            // 
            // nudTTL
            // 
            this.nudTTL.Location = new System.Drawing.Point(315, 439);
            this.nudTTL.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudTTL.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudTTL.Name = "nudTTL";
            this.nudTTL.Size = new System.Drawing.Size(67, 20);
            this.nudTTL.TabIndex = 7;
            this.nudTTL.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // FConfigCustomDomainMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 471);
            this.Controls.Add(this.nudTTL);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FConfigCustomDomainMapping";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Custom Domain Mapping Configuration";
            this.Shown += new System.EventHandler(this.FConfigCustomDomainMapping_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTTL)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox lbDomain;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAddDomain;
        private System.Windows.Forms.TextBox txAddDomain;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txEntries;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudTTL;
    }
}