﻿using ARSoft.Tools.Net;
using ARSoft.Tools.Net.Dns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoatlDNS.Resolvers
{
    public interface IDNSResolver
    {
        DnsMessage Resolve(DomainName domainName, RecordType recordType, RecordClass recordClass);
    }
}
