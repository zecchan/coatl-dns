﻿using ARSoft.Tools.Net;
using ARSoft.Tools.Net.Dns;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoatlDNS.Resolvers.GoogleDNS
{
    public class GoogleDNSResolver : IDNSResolver
    {
        public DnsMessage Resolve(DomainName domainName, RecordType recordType, RecordClass recordClass)
        {
            if (domainName == null) return null;

            var url = "https://dns.google.com/resolve?";
            url += "name=" + domainName.ToString();
            url += "&type=" + recordType.ToString();
            url += "&class=" + recordClass.ToString();

            HttpClient client = new HttpClient();
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            DnsMessage message = null;
            var task = Task.Run(async () =>
            {
                var res = await client.GetAsync(url);
                if (res.IsSuccessStatusCode)
                {
                    var json = await res.Content.ReadAsStringAsync();
                    GoogleDNSAnswer rec;
                    try
                    {
                        rec = JsonConvert.DeserializeObject<GoogleDNSAnswer>(json);
                    }
                    catch { throw new Exception("Failed to deserialize DNS Server response"); }

                    message = BuildAnswer(rec);
                }
                else
                {
                    throw new Exception("DNS returned http error " + res.StatusCode);
                }
            });
            task.Wait();
            return message;
        }

        DnsMessage BuildAnswer(GoogleDNSAnswer ans)
        {
            var res = new DnsMessage();

            res.ReturnCode = (ReturnCode)ans.Status;
            res.IsAuthenticData = ans.AD;
            res.IsAuthoritiveAnswer = ans.AA;
            res.IsCheckingDisabled = ans.CD;
            res.IsRecursionAllowed = ans.RA;
            res.IsRecursionDesired = ans.RD;
            res.IsTruncated = ans.TC;
            res.AnswerRecords = ans.Answer.Select(x => BuildRecord(x)).Where(x => x != null).ToList();
            res.AuthorityRecords = ans.Authority.Select(x => BuildRecord(x)).Where(x => x != null).ToList();
            res.AdditionalRecords = ans.Additional.Select(x => BuildRecord(x)).Where(x => x != null).ToList();

            return res;
        }

        DnsRecordBase BuildRecord(GoogleDNSRecord rec)
        {
            var typ = (RecordType)rec.type;
            var sDomain = rec.name.ToDomainName();
            string[] spl;

            switch(typ)
            {
                case RecordType.A:
                    return new ARecord(sDomain, rec.TTL, rec.data.ToIPAddress());
                case RecordType.Aaaa:
                    return new AaaaRecord(sDomain, rec.TTL, rec.data.ToIPAddress());
                case RecordType.CName:
                    return new CNameRecord(sDomain, rec.TTL, rec.data.ToDomainName());
                case RecordType.Ns:
                    return new NsRecord(sDomain, rec.TTL, rec.data.ToDomainName());
                case RecordType.Soa:
                    spl = rec.data?.Split(' ');
                    return new SoaRecord(sDomain, rec.TTL, spl[0].ToDomainName(), spl[1].ToDomainName(), uint.Parse(spl[2]), int.Parse(spl[3]), int.Parse(spl[4]), int.Parse(spl[5]), int.Parse(spl[6]));
                case RecordType.Mx:
                    spl = rec.data?.Split(' ');
                    return new MxRecord(sDomain, rec.TTL, ushort.Parse(spl[0]), spl[1].ToDomainName());
            }

            return null;
        }
    }

    public static class StringExtension
    {
        public static DomainName ToDomainName(this string str)
        {
            return DomainName.Parse(str);
        }

        public static IPAddress ToIPAddress(this string str)
        {
            return IPAddress.Parse(str);
        }
    }

    public class GoogleDNSAnswer
    {
        public int Status { get; set; }

        public bool TC { get; set; }
        public bool RD { get; set; }
        public bool RA { get; set; }
        public bool AD { get; set; }
        public bool AA { get; set; }
        public bool CD { get; set; }

        public List<GoogleDNSRecord> Question { get; set; } = new List<GoogleDNSRecord>();
        public List<GoogleDNSRecord> Answer { get; set; } = new List<GoogleDNSRecord>();
        public List<GoogleDNSRecord> Additional { get; set; } = new List<GoogleDNSRecord>();
        public List<GoogleDNSRecord> Authority { get; set; } = new List<GoogleDNSRecord>();

        public string Comment { get; set; }
    }

    public class GoogleDNSRecord
    {
        public string name { get; set; }
        public int type { get; set; }
        public int TTL { get; set; }
        public string data { get; set; }
    }
}
