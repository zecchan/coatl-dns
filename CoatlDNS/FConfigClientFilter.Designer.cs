﻿namespace CoatlDNS
{
    partial class FConfigClientFilter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FConfigClientFilter));
            this.rbAllowAll = new System.Windows.Forms.RadioButton();
            this.rbBlacklist = new System.Windows.Forms.RadioButton();
            this.rbWhitelist = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txEntries = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbAllowAll
            // 
            this.rbAllowAll.AutoSize = true;
            this.rbAllowAll.Location = new System.Drawing.Point(15, 23);
            this.rbAllowAll.Name = "rbAllowAll";
            this.rbAllowAll.Size = new System.Drawing.Size(98, 17);
            this.rbAllowAll.TabIndex = 1;
            this.rbAllowAll.TabStop = true;
            this.rbAllowAll.Text = "Allow any client";
            this.rbAllowAll.UseVisualStyleBackColor = true;
            this.rbAllowAll.CheckedChanged += new System.EventHandler(this.rbAllowAll_CheckedChanged);
            // 
            // rbBlacklist
            // 
            this.rbBlacklist.AutoSize = true;
            this.rbBlacklist.Location = new System.Drawing.Point(119, 23);
            this.rbBlacklist.Name = "rbBlacklist";
            this.rbBlacklist.Size = new System.Drawing.Size(64, 17);
            this.rbBlacklist.TabIndex = 2;
            this.rbBlacklist.TabStop = true;
            this.rbBlacklist.Text = "Blacklist";
            this.rbBlacklist.UseVisualStyleBackColor = true;
            this.rbBlacklist.CheckedChanged += new System.EventHandler(this.rbBlacklist_CheckedChanged);
            // 
            // rbWhitelist
            // 
            this.rbWhitelist.AutoSize = true;
            this.rbWhitelist.Location = new System.Drawing.Point(189, 23);
            this.rbWhitelist.Name = "rbWhitelist";
            this.rbWhitelist.Size = new System.Drawing.Size(65, 17);
            this.rbWhitelist.TabIndex = 3;
            this.rbWhitelist.TabStop = true;
            this.rbWhitelist.Text = "Whitelist";
            this.rbWhitelist.UseVisualStyleBackColor = true;
            this.rbWhitelist.CheckedChanged += new System.EventHandler(this.rbWhitelist_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbWhitelist);
            this.groupBox1.Controls.Add(this.rbBlacklist);
            this.groupBox1.Controls.Add(this.rbAllowAll);
            this.groupBox1.Location = new System.Drawing.Point(12, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(415, 53);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtering Rule";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txEntries);
            this.groupBox2.Location = new System.Drawing.Point(12, 69);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(415, 359);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "IP Addresses";
            // 
            // txEntries
            // 
            this.txEntries.Location = new System.Drawing.Point(15, 43);
            this.txEntries.Multiline = true;
            this.txEntries.Name = "txEntries";
            this.txEntries.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txEntries.Size = new System.Drawing.Size(384, 300);
            this.txEntries.TabIndex = 0;
            this.txEntries.WordWrap = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(249, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter filtered IP Addresses, one IP Address per line:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(352, 436);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(271, 436);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FConfigClientFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 471);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FConfigClientFilter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Client Filter Configuration";
            this.Load += new System.EventHandler(this.FConfigClientFilter_Load);
            this.Shown += new System.EventHandler(this.FConfigClientFilter_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RadioButton rbAllowAll;
        private System.Windows.Forms.RadioButton rbBlacklist;
        private System.Windows.Forms.RadioButton rbWhitelist;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txEntries;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}