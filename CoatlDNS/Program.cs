﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Management;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows.Forms;

namespace CoatlDNS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(params string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var fi = new FileInfo(Application.ExecutablePath);
            Directory.SetCurrentDirectory(fi.DirectoryName);

            // Set Unset DNS
            if (args.Length > 0)
            {
                if (args[0] == "-setdns")
                {
                    if (args.Length > 2) SetDNS(args[1], args[2]);
                    else
                        if (args.Length > 1) SetDNS(args[1]);
                    return;
                }
                if (args[0] == "-clrdns")
                {
                    if (args.Length > 2) UnsetDNS(args[1], args[2]);
                    else
                    if (args.Length > 1) UnsetDNS(args[1]);
                    return;
                }
            }

            // Reg startup
            if (args.Contains("-regstartup"))
            {
                RegStartup(true);
                return;
            }
            if (args.Contains("-unregstartup"))
            {
                RegStartup(false);
                return;
            }
            Arguments = string.Join(" ", args);

            // Read startup config
            var config = new FMainStartupConfig();
            config.AutoStart = args.Contains("-autostart");
            config.StartMinimized = args.Contains("-min");

            using (var f = new FSplashScreen())
            {
                f.ShowDialog();
            }

            var fmain = new FMain(config);
            ApplicationContext ac = new ApplicationContext(fmain);
            Application.Run(ac);
        }

        static void RegStartup(bool register)
        {
            try
            {
                var skRun = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                if (register)
                {
                    skRun.SetValue("CoatlDNS", "\"" + Application.ExecutablePath + "\" -autostart -min");
                }
                else
                {
                    skRun.DeleteValue("CoatlDNS", false);
                }

                skRun.Close();
            }
            catch
            {
                Environment.Exit(1);
                return;
            }

            Environment.Exit(0);
        }

        static void SetDNS(string id, string ipv6 = null)
        {
            SetDNSServerSearchOrder(id, ipv6, new string[] { "127.0.0.1" }, "::1");
        }

        static void UnsetDNS(string id, string ipv6 = null)
        {
            SetDNSServerSearchOrder(id, ipv6);
        }

        static void SetDNSServerSearchOrder(string id, string ipv6 = null, string[] dnsv4 = null, string dnsv6 = null)
        {
            try
            {
                var nic = NetworkInterface.GetAllNetworkInterfaces().Where(ni => ni.Id == id).FirstOrDefault();
                if (nic == null) throw new Exception();

                ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
                ManagementObjectCollection objMOC = objMC.GetInstances();
                foreach (ManagementObject objMO in objMOC)
                {
                    try
                    {
                        if ((bool)objMO["IPEnabled"])
                        {
                            var nicid = objMO["SettingID"];
                            if (nicid.ToString().ToLower() == id.ToLower())
                            {
                                ManagementBaseObject objdns = objMO.GetMethodParameters("SetDNSServerSearchOrder");
                                if (objdns != null)
                                {
                                    objdns["DNSServerSearchOrder"] = dnsv4;
                                    objMO.InvokeMethod("SetDNSServerSearchOrder", objdns, null);
                                }
                            }
                        }
                    }
                    catch { }
                }
            }
            catch
            {
                Environment.Exit(1);
                return;
            }

            try
            {
                if (ipv6 != null)
                {
                    var ps = "Set-DnsClientServerAddress -InterfaceAlias \"" + ipv6 + "\" ";
                    if (dnsv6 != null) ps += "-ServerAddresses \"" + dnsv6 + "\"";
                    else ps += "-ResetServerAddresses";
                    RunPowerScript(ps);
                }
            }
            catch { }
            Environment.Exit(0);
        }

        public static string Arguments { get; private set; }

        private static string RunPowerScript(string scriptText)
        {
            // create Powershell runspace

            Runspace runspace = RunspaceFactory.CreateRunspace();

            // open it

            runspace.Open();

            // create a pipeline and feed it the script text

            Pipeline pipeline = runspace.CreatePipeline();
            pipeline.Commands.AddScript(scriptText);

            // add an extra command to transform the script
            // output objects into nicely formatted strings

            // remove this line to get the actual objects
            // that the script returns. For example, the script

            // "Get-Process" returns a collection
            // of System.Diagnostics.Process instances.

            pipeline.Commands.Add("Out-String");

            // execute the script

            var results = pipeline.Invoke();

            // close the runspace

            runspace.Close();

            // convert the script result into a single string

            StringBuilder stringBuilder = new StringBuilder();
            foreach (PSObject obj in results)
            {
                stringBuilder.AppendLine(obj.ToString());
            }

            return stringBuilder.ToString();
        }
    }
}
