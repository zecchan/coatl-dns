﻿namespace CoatlDNS
{
    partial class FSetupInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSetupInterface));
            this.label1 = new System.Windows.Forms.Label();
            this.ddNetInterface = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lbNICDNS = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbNICGateway = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbNICIPAddr = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbNICType = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbNICDesc = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbNICName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lbNICIPPrio = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbNICIPAddrV6 = new System.Windows.Forms.Label();
            this.lbNICGatewayV6 = new System.Windows.Forms.Label();
            this.lbNICIPPrioV6 = new System.Windows.Forms.Label();
            this.lbNICStatus = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select your network interface:";
            // 
            // ddNetInterface
            // 
            this.ddNetInterface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddNetInterface.FormattingEnabled = true;
            this.ddNetInterface.Location = new System.Drawing.Point(181, 19);
            this.ddNetInterface.Name = "ddNetInterface";
            this.ddNetInterface.Size = new System.Drawing.Size(385, 21);
            this.ddNetInterface.TabIndex = 1;
            this.ddNetInterface.SelectedIndexChanged += new System.EventHandler(this.ddNetInterface_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbNICStatus);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.lbNICIPPrioV6);
            this.groupBox1.Controls.Add(this.lbNICGatewayV6);
            this.groupBox1.Controls.Add(this.lbNICIPAddrV6);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.lbNICIPPrio);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.lbNICDNS);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.lbNICGateway);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lbNICIPAddr);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lbNICType);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lbNICDesc);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lbNICName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(30, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(536, 361);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Network Interface Information";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(367, 311);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(154, 34);
            this.button3.TabIndex = 15;
            this.button3.Text = "Reset to default";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(207, 311);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(154, 34);
            this.button2.TabIndex = 14;
            this.button2.Text = "Setup Coatl DNS";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lbNICDNS
            // 
            this.lbNICDNS.AutoSize = true;
            this.lbNICDNS.Location = new System.Drawing.Point(104, 193);
            this.lbNICDNS.Name = "lbNICDNS";
            this.lbNICDNS.Size = new System.Drawing.Size(66, 13);
            this.lbNICDNS.TabIndex = 13;
            this.lbNICDNS.Text = "<NIC_DNS>";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(20, 193);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "DNS";
            // 
            // lbNICGateway
            // 
            this.lbNICGateway.AutoSize = true;
            this.lbNICGateway.Location = new System.Drawing.Point(104, 133);
            this.lbNICGateway.Name = "lbNICGateway";
            this.lbNICGateway.Size = new System.Drawing.Size(97, 13);
            this.lbNICGateway.TabIndex = 11;
            this.lbNICGateway.Text = "<NIC_GATEWAY>";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(20, 133);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Gateway";
            // 
            // lbNICIPAddr
            // 
            this.lbNICIPAddr.AutoSize = true;
            this.lbNICIPAddr.Location = new System.Drawing.Point(104, 111);
            this.lbNICIPAddr.Name = "lbNICIPAddr";
            this.lbNICIPAddr.Size = new System.Drawing.Size(84, 13);
            this.lbNICIPAddr.TabIndex = 7;
            this.lbNICIPAddr.Text = "<NIC_IPADDR>";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(20, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "IP Addresses";
            // 
            // lbNICType
            // 
            this.lbNICType.AutoSize = true;
            this.lbNICType.Location = new System.Drawing.Point(104, 73);
            this.lbNICType.Name = "lbNICType";
            this.lbNICType.Size = new System.Drawing.Size(71, 13);
            this.lbNICType.TabIndex = 5;
            this.lbNICType.Text = "<NIC_TYPE>";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(20, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Type";
            // 
            // lbNICDesc
            // 
            this.lbNICDesc.AutoSize = true;
            this.lbNICDesc.Location = new System.Drawing.Point(104, 51);
            this.lbNICDesc.Name = "lbNICDesc";
            this.lbNICDesc.Size = new System.Drawing.Size(72, 13);
            this.lbNICDesc.TabIndex = 3;
            this.lbNICDesc.Text = "<NIC_DESC>";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(20, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Description";
            // 
            // lbNICName
            // 
            this.lbNICName.AutoSize = true;
            this.lbNICName.Location = new System.Drawing.Point(104, 29);
            this.lbNICName.Name = "lbNICName";
            this.lbNICName.Size = new System.Drawing.Size(74, 13);
            this.lbNICName.TabIndex = 1;
            this.lbNICName.Text = "<NIC_NAME>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(20, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(491, 424);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbNICIPPrio
            // 
            this.lbNICIPPrio.AutoSize = true;
            this.lbNICIPPrio.Location = new System.Drawing.Point(104, 155);
            this.lbNICIPPrio.Name = "lbNICIPPrio";
            this.lbNICIPPrio.Size = new System.Drawing.Size(79, 13);
            this.lbNICIPPrio.TabIndex = 17;
            this.lbNICIPPrio.Text = "<NIC_IPPRIO>";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.Location = new System.Drawing.Point(20, 155);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Index";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(20, 233);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(473, 52);
            this.label12.TabIndex = 21;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // lbNICIPAddrV6
            // 
            this.lbNICIPAddrV6.AutoSize = true;
            this.lbNICIPAddrV6.Location = new System.Drawing.Point(223, 111);
            this.lbNICIPAddrV6.Name = "lbNICIPAddrV6";
            this.lbNICIPAddrV6.Size = new System.Drawing.Size(84, 13);
            this.lbNICIPAddrV6.TabIndex = 22;
            this.lbNICIPAddrV6.Text = "<NIC_IPADDR>";
            // 
            // lbNICGatewayV6
            // 
            this.lbNICGatewayV6.AutoSize = true;
            this.lbNICGatewayV6.Location = new System.Drawing.Point(223, 133);
            this.lbNICGatewayV6.Name = "lbNICGatewayV6";
            this.lbNICGatewayV6.Size = new System.Drawing.Size(97, 13);
            this.lbNICGatewayV6.TabIndex = 23;
            this.lbNICGatewayV6.Text = "<NIC_GATEWAY>";
            // 
            // lbNICIPPrioV6
            // 
            this.lbNICIPPrioV6.AutoSize = true;
            this.lbNICIPPrioV6.Location = new System.Drawing.Point(223, 155);
            this.lbNICIPPrioV6.Name = "lbNICIPPrioV6";
            this.lbNICIPPrioV6.Size = new System.Drawing.Size(79, 13);
            this.lbNICIPPrioV6.TabIndex = 24;
            this.lbNICIPPrioV6.Text = "<NIC_IPPRIO>";
            // 
            // lbNICStatus
            // 
            this.lbNICStatus.AutoSize = true;
            this.lbNICStatus.Location = new System.Drawing.Point(280, 73);
            this.lbNICStatus.Name = "lbNICStatus";
            this.lbNICStatus.Size = new System.Drawing.Size(86, 13);
            this.lbNICStatus.TabIndex = 26;
            this.lbNICStatus.Text = "<NIC_STATUS>";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(223, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Status";
            // 
            // FSetupInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 459);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ddNetInterface);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FSetupInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Setup Network Interface";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ddNetInterface;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbNICName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbNICDesc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbNICType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbNICIPAddr;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbNICGateway;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbNICDNS;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label lbNICIPPrio;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbNICIPAddrV6;
        private System.Windows.Forms.Label lbNICGatewayV6;
        private System.Windows.Forms.Label lbNICIPPrioV6;
        private System.Windows.Forms.Label lbNICStatus;
        private System.Windows.Forms.Label label7;
    }
}