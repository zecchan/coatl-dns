﻿using ARSoft.Tools.Net;
using ARSoft.Tools.Net.Dns;
using CoatlDNS.Resolvers;
using CoatlDNS.Resolvers.GoogleDNS;
using CoatlDNSCore;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Windows.Forms;

namespace CoatlDNS
{
    public partial class FMain : Form
    {
        Version Version { get; } = CoatlDNSServer.Version;
        FMainStartupConfig StartupConfig;
        CoatlDNSConfig AppConfig { get; set; }
        CoatlDNSServer Server { get; }

        public FMain(FMainStartupConfig config)
        {
            InitializeComponent();

            lbCoatlDNS.Text = "CoatlDNS v" + Version.ToString();

            StartupConfig = config;
            try
            {
                txLog.Font = new Font("Consolas", 8f);
            }
            catch
            {
                try
                {
                    txLog.Font = new Font("Lucida Console", 7f);
                }
                catch
                {
                    txLog.Font = new Font(FontFamily.GenericMonospace, 8f);
                }
            }
            ddLogLevel.SelectedIndex = 2;

            List<IPAddress> allInterfaceIPAddress = new List<IPAddress>();
            foreach (NetworkInterface netif in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (netif.OperationalStatus == OperationalStatus.Up && !netif.IsReceiveOnly)
                {
                    var ip4stat = netif.GetIPv4Statistics();
                    var ip6stat = netif.GetIPStatistics();
                    IPInterfaceProperties properties = netif.GetIPProperties();
                    foreach (IPAddressInformation address in properties.UnicastAddresses)
                        if (address.IsDnsEligible)
                            allInterfaceIPAddress.Add(address.Address);
                }
            }

            // add loopbacks
            if (!allInterfaceIPAddress.Contains(IPAddress.Loopback))
                allInterfaceIPAddress.Add(IPAddress.Loopback);
            if (!allInterfaceIPAddress.Contains(IPAddress.IPv6Loopback))
                allInterfaceIPAddress.Add(IPAddress.IPv6Loopback);

            Server = new CoatlDNSServer(allInterfaceIPAddress.Select(x => new IPEndPoint(x, 53)).ToArray());

            Server.OnClientConnected += Server_OnClientConnected;
            Server.OnClientConnectionBlocked += Server_OnClientConnectionBlocked;
            Server.OnQueryReceived += Server_OnQueryReceived;
            Server.OnResolveRequest += Server_OnResolveRequest;
            Server.OnQueryFailed += Server_OnQueryFailed;
            Server.OnAnswerSent += Server_OnAnswerSent;
            Server.OnDomainBlacklisted += Server_OnDomainBlacklisted;
            Server.OnDomainNotWhitelisted += Server_OnDomainNotWhitelisted;

            LoadServerConfig();
        }

        #region Server Related
        private struct LogEntry
        {
            public LogLevel level;
            public string message;
            public DateTime dateTime;

            public string ToLog()
            {
                return $"[{level.ToString().PadRight(8, ' ')} - {dateTime.ToString("yyyy-MM-dd HH:mm:ss")}] {message}";
            }
        }
        private Queue<LogEntry> Log { get; } = new Queue<LogEntry>();
        private void AppendLog(LogLevel level, string message)
        {
            Log.Enqueue(new LogEntry()
            {
                level = level,
                message = message,
                dateTime = DateTime.Now
            });
            //RefreshLog();
        }

        int lastLog = -1;
        DateTime lastLogTime = DateTime.MinValue;
        private void _refreshLog()
        {
            while (Log.Count > 500) Log.Dequeue();

            try
            {
                var flog = Log.Where(l => (int)l.level >= ddLogLevel.SelectedIndex).ToList();
                if (lastLog == flog.Count && lastLogTime >= Log.Max(x => x.dateTime))
                    return;
                var log = string.Join("\r\n", flog.Select(l => l.ToLog()));
                txLog.Clear();
                txLog.AppendText(log);
                lbEntries.Text = $"({flog.Count()} of {Log.Count} entries)";
                lastLog = flog.Count;
                lastLogTime = Log.Max(x => x.dateTime);
            }
            catch { }
        }
        private void RefreshLog()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    _refreshLog();
                }));
            }
            else
            {
                _refreshLog();
            }
        }

        private void Server_OnDomainBlacklisted(object sender, IPEndPoint remoteEndPoint, DomainName domainName, RecordType recordType, RecordClass recordClass)
        {
            string msg = $"[{domainName}] is blacklisted, sending NxDomain to [{remoteEndPoint.Address.ToString()}]";
            AppendLog(LogLevel.Warning, msg);
        }

        private void Server_OnDomainNotWhitelisted(object sender, IPEndPoint remoteEndPoint, DomainName domainName, RecordType recordType, RecordClass recordClass)
        {
            string msg = $"[{domainName}] is not in whitelist, sending NxDomain to [{remoteEndPoint.Address.ToString()}]";
            AppendLog(LogLevel.Warning, msg);
        }

        private void Server_OnAnswerSent(object sender, IPEndPoint remoteEndPoint, DomainName domainName, RecordType recordType, RecordClass recordClass)
        {
            string msg = $"[{recordType}] record of [{domainName}] is sent to [{remoteEndPoint.Address.ToString()}]";
            AppendLog(LogLevel.Notice, msg);
        }

        private void Server_OnQueryFailed(object sender, IPEndPoint remoteEndPoint, DomainName domainName, RecordType recordType, RecordClass recordClass)
        {
            string msg = $"Failed to resolve [{recordType}] record of [{domainName}] for [{remoteEndPoint.Address.ToString()}]";
            AppendLog(LogLevel.Warning, msg);
        }

        private void Server_OnQueryReceived(object sender, IPEndPoint remoteEndPoint, DomainName domainName, RecordType recordType, RecordClass recordClass)
        {
            string msg = $"[{remoteEndPoint.Address.ToString()}] requested [{recordType}] record of [{domainName}]";
            AppendLog(LogLevel.Notice, msg);
        }

        private void Server_OnClientConnected(object sender, IPEndPoint remoteEndPoint)
        {
            string msg = $"[{remoteEndPoint.Address.ToString()}] initiated a connection";
            AppendLog(LogLevel.Debug, msg);
        }

        private void Server_OnClientConnectionBlocked(object sender, IPEndPoint remoteEndPoint, string blockReason = null)
        {
            string msg = $"[{remoteEndPoint.Address.ToString()}] connection is blocked ({blockReason})";
            AppendLog(LogLevel.Notice, msg);
        }

        public IDNSResolver Resolver = new GoogleDNSResolver();

        private DnsMessage Server_OnResolveRequest(object sender, DomainName domainName, RecordType recordType, RecordClass recordClass)
        {
            string msg = $"Resolving [{recordType}] record of [{domainName}]...";
            AppendLog(LogLevel.Notice, msg);
            try
            {
                return Resolver.Resolve(domainName, recordType, recordClass);
            }
            catch (Exception ex)
            {
                AppendLog(LogLevel.Error, $"Error when resolving [{domainName}]: {ex.Message}");
                return null;
            }
        }
        #endregion

        bool firstTimeShown = true;
        private void FMain_Shown(object sender, System.EventArgs e)
        {
            if (firstTimeShown)
            {
                if (StartupConfig.StartMinimized)
                    WindowState = FormWindowState.Minimized;
                else
                    WindowState = FormWindowState.Normal;
                if (StartupConfig.AutoStart)
                    btnStart_Click(btnStart, null);
                firstTimeShown = false;
                return;
            }
        }

        private void ToTray(bool showNotif)
        {
            Hide();
            niTray.BalloonTipText = "Coatl DNS is minimized to system tray, double click the icon to open.";
            niTray.BalloonTipTitle = "Coatl DNS";
            niTray.Visible = true;
            if (showNotif)
                niTray.ShowBalloonTip(1000);
        }

        bool bypassExitConfirmation = false;
        private void Restart(bool autoRun)
        {
            bypassExitConfirmation = true;

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.UseShellExecute = true;
            startInfo.WorkingDirectory = Environment.CurrentDirectory;
            startInfo.FileName = Application.ExecutablePath;
            startInfo.Arguments = autoRun ? "-autostart" : "";
            Process.Start(startInfo);

            Application.Exit();
        }

        private void niTray_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            this.WindowState = FormWindowState.Normal;
            niTray.Visible = false;
        }

        private void FMain_Resize(object sender, System.EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                if (AppConfig.MinimizeToTray)
                {
                    ToTray(firstTimeShown == false);
                }
            }
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show("Do you want to restart Coatl DNS?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var isRunning = Server.IsRunning;
                Server.Stop();
                Restart(isRunning);
            }
        }

        private void FMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool isShuttingDown = false;
            try
            {
                isShuttingDown = WinInterop.GetSystemMetrics(WinInterop.SM_SHUTTINGDOWN) != 0;
            }
            catch { }
            if (!isShuttingDown && !bypassExitConfirmation)
            {
                if (MessageBox.Show("Do you want to exit Coatl DNS?\r\n(Server will be stopped)", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    e.Cancel = true;
                    return;
                }
            }
            Server.Stop();
        }

        private void LoadServerConfig()
        {
            AppConfig = new CoatlDNSConfig("config.cfg", Server);

            try
            {
                var dn = DomainName.Parse("dns.google.com");
                var dn2 = DomainName.Parse("dns.google");
                List<IPAddress> addrs = new List<IPAddress>();
                if (File.Exists("dns.google.com.txt"))
                {
                    using (var fi = File.OpenText("dns.google.com.txt"))
                    {
                        string line = null;
                        while ((line = fi.ReadLine()) != null)
                        {
                            if (string.IsNullOrWhiteSpace(line)) continue;
                            try
                            {
                                var ip = IPAddress.Parse(line.Trim());
                                addrs.Add(ip);
                            }
                            catch { }
                        }
                    }
                }
                if (addrs.Count == 0)
                {
                    try
                    {
                        addrs.Add(IPAddress.Parse("8.8.8.8"));
                        addrs.Add(IPAddress.Parse("8.8.4.4"));
                        addrs.Add(IPAddress.Parse("172.217.194.113"));
                        addrs.Add(IPAddress.Parse("172.217.194.138"));
                        addrs.Add(IPAddress.Parse("172.217.194.100"));
                        addrs.Add(IPAddress.Parse("172.217.194.101"));
                        addrs.Add(IPAddress.Parse("172.217.194.139"));
                        addrs.Add(IPAddress.Parse("172.217.194.102"));
                    }
                    catch { }
                }
                if (!Server.CustomDomainMapping.ContainsKey(dn) || Server.CustomDomainMapping[dn].Length == 0)
                {
                    Server.CustomDomainMapping[dn] = addrs.ToArray();
                }
                if (!Server.CustomDomainMapping.ContainsKey(dn2) || Server.CustomDomainMapping[dn2].Length == 0)
                {
                    Server.CustomDomainMapping[dn2] = addrs.ToArray();
                }
            }
            catch { }
        }

        private void btnStart_Click(object sender, System.EventArgs e)
        {
            if (Server.IsRunning)
            {
                Server.Stop();
                btnStart.Text = "Start Server";
                btnStart.BackColor = Color.LimeGreen;
                AppendLog(LogLevel.Notice, "Server stopped");
            }
            else
            {
                try
                {
                    AppendLog(LogLevel.Verbose, $"Starting server... ({string.Join(", ", Server.BoundEndPoints.Select(x => x.ToString()))})");
                    txLog.Clear();
                    Server.Start();
                    btnStart.Text = "Stop Server";
                    btnStart.BackColor = Color.Red;
                    AppendLog(LogLevel.Notice, "Server started");
                }
                catch (SocketException ex)
                {
                    if (ex.ErrorCode == 10048)
                        AppendLog(LogLevel.Error, $"Port 53 is already bound to another application");
                    else
                        AppendLog(LogLevel.Error, ex.Message);
                }
                catch (Exception ex)
                {
                    AppendLog(LogLevel.Error, ex.Message);
                }
            }
        }

        private void llClearLog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Log.Clear();
                RefreshLog();
            }
            catch { }
        }

        private void llConfigCoatlDNS_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (var f = new FConfigCoatlDNS())
            {
                f.Config = AppConfig;
                f.ShowDialog();
            }
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Server.ClearCache();
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/C ipconfig /flushdns";
                startInfo.UseShellExecute = true;
                startInfo.Verb = "runas";
                var p = Process.Start(startInfo);
                p.WaitForExit();
                if (p.ExitCode != 0)
                    throw new Exception();
                MessageBox.Show("Successfully flushed DNS Cache", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Failed to flush DNS Cache", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void llInterface_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (var f = new FSetupInterface())
            {
                f.ShowDialog();
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (var f = new FConfigClientFilter())
            {
                f.AppConfig = AppConfig;
                f.ShowDialog();
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (var f = new FConfigDomainFilter())
            {
                f.AppConfig = AppConfig;
                f.ShowDialog();
            }
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (var f = new FConfigCustomDomainMapping())
            {
                f.AppConfig = AppConfig;
                f.ShowDialog();
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Click(object sender, EventArgs e)
        {
            Process.Start("https://code-atelier.net/");
        }

        private void ddLogLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                RefreshLog();
            }
            catch { }
        }

        private void tmrLog_Tick(object sender, EventArgs e)
        {
            RefreshLog();
        }
    }

    public struct FMainStartupConfig
    {
        public bool AutoStart;
        public bool StartMinimized;
    }

    public class CoatlDNSConfig
    {
        public string Path { get; }
        public CoatlDNSServer Server { get; }

        public bool CanWrite(string path)
        {
            try
            {
                var dir = System.IO.Path.GetDirectoryName(path) + "\\";
                using (var fo = File.CreateText(dir + "test"))
                {
                    fo.WriteLine("TEST");
                }
                File.Delete(dir + "test");
                return true;
            }
            catch { return false; }
        }

        public CoatlDNSConfig(string path, CoatlDNSServer server)
        {
            Path = System.IO.Path.GetFullPath(path);

            if (!CanWrite(Path))
            {
                var localDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\CodeAtelier\\CoatlDNS\\";
                if (!Directory.Exists(localDir))
                    Directory.CreateDirectory(localDir);
                Path = localDir + System.IO.Path.GetFileName(path);
            }
            Server = server;

            // Default Values
            server.ClientBlacklist.Clear();
            server.ClientWhitelist.Clear();
            server.ClientFilterMode = FilterMode.AllowAll;
            server.DomainBlacklist.Clear();
            server.DomainWhitelist.Clear();
            server.DomainFilterMode = FilterMode.AllowAll;
            server.CustomDomainMapping.Clear();
            server.CustomDomainMappingTTL = 200;
            server.UseCache = true;
            server.MaximumCacheCount = 1000;
            MinimizeToTray = true;

            try
            {
                int mode = 0;
                using (var fi = File.OpenText(Path))
                {
                    string line = null;
                    while ((line = fi.ReadLine()) != null)
                    {
                        // Check mode changes
                        line = line.Trim();
                        if (line == "=== Client Blacklist ===")
                        {
                            mode = 1;
                            continue;
                        }
                        if (line == "=== Client Whitelist ===")
                        {
                            mode = 2;
                            continue;
                        }
                        if (line == "=== Domain Blacklist ===")
                        {
                            mode = 3;
                            continue;
                        }
                        if (line == "=== Domain Whitelist ===")
                        {
                            mode = 4;
                            continue;
                        }
                        if (line == "=== Custom Domain Mapping ===")
                        {
                            mode = 6;
                            continue;
                        }

                        // Key based config
                        if (mode == 0)
                        {
                            var spl = line.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries).Select(sub => sub.Trim()).ToList();
                            if (spl.Count == 2)
                            {
                                var key = spl[0];
                                var value = spl[1];
                                if (key == "MinimizeToTray")
                                    MinimizeToTray = value == "true";
                                if (key == "Name")
                                {
                                    if (!string.IsNullOrWhiteSpace(value))
                                    {
                                        DomainName dn = null;
                                        if (DomainName.TryParse(value, out dn))
                                            server.Name = dn;
                                    }
                                    else server.Name = null;
                                }
                                if (key == "ClientFilterRule")
                                {
                                    if (value == "blacklist") server.ClientFilterMode = FilterMode.Blacklist;
                                    if (value == "whitelist") server.ClientFilterMode = FilterMode.Whitelist;
                                }
                                if (key == "DomainFilterRule")
                                {
                                    if (value == "blacklist") server.DomainFilterMode = FilterMode.Blacklist;
                                    if (value == "whitelist") server.DomainFilterMode = FilterMode.Whitelist;
                                }
                                if (key == "CustomDomainMappingTTL")
                                {
                                    int ttl = 200;
                                    if (int.TryParse(value, out ttl))
                                        server.CustomDomainMappingTTL = ttl;
                                    else
                                        server.CustomDomainMappingTTL = 200;
                                }
                                if (key == "CacheDNSMessage")
                                {
                                    server.UseCache = value == "true";
                                }
                                if (key == "DNSCacheLimit")
                                {
                                    int lim = 200;
                                    if (int.TryParse(value, out lim))
                                        server.MaximumCacheCount = lim;
                                    else
                                        server.MaximumCacheCount = lim;
                                }
                            }
                        }

                        // Line based Client Filter
                        if (mode == 1 || mode == 2)
                        {
                            if (!string.IsNullOrWhiteSpace(line))
                            {
                                try
                                {
                                    var ip = IPAddress.Parse(line);
                                    if (mode == 1)
                                        server.ClientBlacklist.Add(ip);
                                    else
                                        server.ClientWhitelist.Add(ip);
                                }
                                catch { }
                            }
                        }

                        // Line based Domain Filter
                        if (mode == 3 || mode == 4)
                        {
                            if (!string.IsNullOrWhiteSpace(line))
                            {
                                try
                                {
                                    var dn = DomainName.Parse(line);
                                    if (mode == 3)
                                        server.DomainBlacklist.Add(dn);
                                    else
                                        server.DomainWhitelist.Add(dn);
                                }
                                catch { }
                            }
                        }

                        // Line based Custom Domain Mapping
                        if (mode == 6)
                        {
                            if (!string.IsNullOrWhiteSpace(line))
                            {
                                try
                                {
                                    var spl = line.Split(new char[] { '>' }, 2, StringSplitOptions.RemoveEmptyEntries).Select(sub => sub.Trim()).ToList();
                                    if (spl.Count == 2)
                                    {
                                        var domain = DomainName.Parse(spl[0]);
                                        var ipstr = spl[1].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(sub => sub.Trim()).ToArray();
                                        var ipaddr = ipstr.Select(ip => IPAddress.Parse(ip)).ToArray();
                                        server.CustomDomainMapping.Add(domain, ipaddr);
                                    }
                                }
                                catch { }
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        public void Save()
        {
            try
            {
                using (var fi = File.CreateText(Path))
                {
                    fi.WriteLine("MinimizeToTray=" + (MinimizeToTray ? "true" : "false"));
                    if (Server.ClientFilterMode != FilterMode.AllowAll)
                        fi.WriteLine("ClientFilterRule=" + (Server.ClientFilterMode == FilterMode.Blacklist ? "blacklist" : "whitelist"));
                    if (Server.DomainFilterMode != FilterMode.AllowAll)
                        fi.WriteLine("DomainFilterRule=" + (Server.DomainFilterMode == FilterMode.Blacklist ? "blacklist" : "whitelist"));
                    fi.WriteLine("CustomDomainMappingTTL=" + Server.CustomDomainMappingTTL.ToString());
                    fi.WriteLine("CacheDNSMessage=" + (Server.UseCache ? "true" : "false"));
                    fi.WriteLine("DNSCacheLimit=" + Server.MaximumCacheCount.ToString());
                    if (Server.Name != null)
                        fi.WriteLine("Name=" + Server.Name.ToString());

                    // Write Client Filter
                    fi.WriteLine("=== Client Blacklist ===");
                    foreach (var ip in Server.ClientBlacklist)
                    {
                        fi.WriteLine(ip.ToString());
                    }
                    fi.WriteLine("=== Client Whitelist ===");
                    foreach (var ip in Server.ClientWhitelist)
                    {
                        fi.WriteLine(ip.ToString());
                    }

                    // Write Domain Filter
                    fi.WriteLine("=== Domain Blacklist ===");
                    foreach (var ip in Server.DomainBlacklist)
                    {
                        fi.WriteLine(ip.ToString());
                    }
                    fi.WriteLine("=== Domain Whitelist ===");
                    foreach (var ip in Server.DomainWhitelist)
                    {
                        fi.WriteLine(ip.ToString());
                    }

                    // Write Custom Domain Mapping
                    fi.WriteLine("=== Custom Domain Mapping ===");
                    foreach (var mp in Server.CustomDomainMapping)
                    {
                        fi.WriteLine(mp.Key.ToString() + ">" + string.Join(";", mp.Value.Select(ip => ip.ToString())));
                    }
                }
            }
            catch
            {

            }
        }

        public bool MinimizeToTray { get; set; }
    }
}
