﻿namespace CoatlDNS
{
    partial class FMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMain));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbEntries = new System.Windows.Forms.Label();
            this.ddLogLevel = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.llClearLog = new System.Windows.Forms.LinkLabel();
            this.txLog = new System.Windows.Forms.TextBox();
            this.niTray = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbCoatlDNS = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.llConfigCoatlDNS = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.llInterface = new System.Windows.Forms.LinkLabel();
            this.btnStart = new System.Windows.Forms.Button();
            this.tmrLog = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbEntries);
            this.groupBox1.Controls.Add(this.ddLogLevel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.llClearLog);
            this.groupBox1.Controls.Add(this.txLog);
            this.groupBox1.Location = new System.Drawing.Point(206, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(700, 392);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Server Logs";
            // 
            // lbEntries
            // 
            this.lbEntries.AutoSize = true;
            this.lbEntries.Location = new System.Drawing.Point(341, 366);
            this.lbEntries.Name = "lbEntries";
            this.lbEntries.Size = new System.Drawing.Size(74, 13);
            this.lbEntries.TabIndex = 11;
            this.lbEntries.Text = "(0 of 0 entries)";
            // 
            // ddLogLevel
            // 
            this.ddLogLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddLogLevel.FormattingEnabled = true;
            this.ddLogLevel.Items.AddRange(new object[] {
            "Verbose",
            "Debug",
            "Notice",
            "Warning",
            "Error",
            "Fatal Error"});
            this.ddLogLevel.Location = new System.Drawing.Point(214, 363);
            this.ddLogLevel.Name = "ddLogLevel";
            this.ddLogLevel.Size = new System.Drawing.Size(121, 21);
            this.ddLogLevel.TabIndex = 10;
            this.ddLogLevel.SelectedIndexChanged += new System.EventHandler(this.ddLogLevel_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 366);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Show log which level is above or equal to";
            // 
            // llClearLog
            // 
            this.llClearLog.AutoSize = true;
            this.llClearLog.Location = new System.Drawing.Point(637, 366);
            this.llClearLog.Name = "llClearLog";
            this.llClearLog.Size = new System.Drawing.Size(57, 13);
            this.llClearLog.TabIndex = 8;
            this.llClearLog.TabStop = true;
            this.llClearLog.Text = "Clear Logs";
            this.llClearLog.VisitedLinkColor = System.Drawing.Color.Blue;
            this.llClearLog.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llClearLog_LinkClicked);
            // 
            // txLog
            // 
            this.txLog.Location = new System.Drawing.Point(6, 19);
            this.txLog.Multiline = true;
            this.txLog.Name = "txLog";
            this.txLog.ReadOnly = true;
            this.txLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txLog.Size = new System.Drawing.Size(688, 338);
            this.txLog.TabIndex = 0;
            this.txLog.WordWrap = false;
            // 
            // niTray
            // 
            this.niTray.Icon = ((System.Drawing.Icon)(resources.GetObject("niTray.Icon")));
            this.niTray.Text = "Coatl DNS";
            this.niTray.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.niTray_MouseDoubleClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.lbCoatlDNS);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.linkLabel5);
            this.panel1.Controls.Add(this.llConfigCoatlDNS);
            this.panel1.Controls.Add(this.linkLabel3);
            this.panel1.Controls.Add(this.linkLabel2);
            this.panel1.Controls.Add(this.linkLabel1);
            this.panel1.Controls.Add(this.llInterface);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 404);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel2.Location = new System.Drawing.Point(8, 11);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(181, 49);
            this.panel2.TabIndex = 11;
            this.panel2.Click += new System.EventHandler(this.panel2_Click);
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // lbCoatlDNS
            // 
            this.lbCoatlDNS.BackColor = System.Drawing.Color.Transparent;
            this.lbCoatlDNS.Location = new System.Drawing.Point(8, 365);
            this.lbCoatlDNS.Name = "lbCoatlDNS";
            this.lbCoatlDNS.Size = new System.Drawing.Size(177, 13);
            this.lbCoatlDNS.TabIndex = 10;
            this.lbCoatlDNS.Text = "Coatl DNS v1.4";
            this.lbCoatlDNS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(12, 380);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Copyright © 2022 - Code Atelier";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.AliceBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.button1.Location = new System.Drawing.Point(11, 332);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(174, 26);
            this.button1.TabIndex = 8;
            this.button1.Text = "Restart Application";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // linkLabel5
            // 
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.linkLabel5.LinkColor = System.Drawing.Color.DarkBlue;
            this.linkLabel5.Location = new System.Drawing.Point(11, 251);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(119, 17);
            this.linkLabel5.TabIndex = 7;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "Flush DNS Cache";
            this.linkLabel5.VisitedLinkColor = System.Drawing.Color.DarkBlue;
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
            // 
            // llConfigCoatlDNS
            // 
            this.llConfigCoatlDNS.AutoSize = true;
            this.llConfigCoatlDNS.BackColor = System.Drawing.Color.Transparent;
            this.llConfigCoatlDNS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.llConfigCoatlDNS.LinkColor = System.Drawing.Color.DarkBlue;
            this.llConfigCoatlDNS.Location = new System.Drawing.Point(11, 227);
            this.llConfigCoatlDNS.Name = "llConfigCoatlDNS";
            this.llConfigCoatlDNS.Size = new System.Drawing.Size(138, 17);
            this.llConfigCoatlDNS.TabIndex = 6;
            this.llConfigCoatlDNS.TabStop = true;
            this.llConfigCoatlDNS.Text = "Configure Coatl DNS";
            this.llConfigCoatlDNS.VisitedLinkColor = System.Drawing.Color.DarkBlue;
            this.llConfigCoatlDNS.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llConfigCoatlDNS_LinkClicked);
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.linkLabel3.LinkColor = System.Drawing.Color.DarkBlue;
            this.linkLabel3.Location = new System.Drawing.Point(11, 203);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(178, 17);
            this.linkLabel3.TabIndex = 5;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Configure Custom Mapping";
            this.linkLabel3.VisitedLinkColor = System.Drawing.Color.DarkBlue;
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.linkLabel2.LinkColor = System.Drawing.Color.DarkBlue;
            this.linkLabel2.Location = new System.Drawing.Point(11, 179);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(156, 17);
            this.linkLabel2.TabIndex = 4;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Configure Domain Filter";
            this.linkLabel2.VisitedLinkColor = System.Drawing.Color.DarkBlue;
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.linkLabel1.LinkColor = System.Drawing.Color.DarkBlue;
            this.linkLabel1.Location = new System.Drawing.Point(11, 155);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(143, 17);
            this.linkLabel1.TabIndex = 3;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Configure Client Filter";
            this.linkLabel1.VisitedLinkColor = System.Drawing.Color.DarkBlue;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // llInterface
            // 
            this.llInterface.AutoSize = true;
            this.llInterface.BackColor = System.Drawing.Color.Transparent;
            this.llInterface.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.llInterface.LinkColor = System.Drawing.Color.DarkBlue;
            this.llInterface.Location = new System.Drawing.Point(11, 131);
            this.llInterface.Name = "llInterface";
            this.llInterface.Size = new System.Drawing.Size(159, 17);
            this.llInterface.TabIndex = 2;
            this.llInterface.TabStop = true;
            this.llInterface.Text = "Setup Network Interface";
            this.llInterface.VisitedLinkColor = System.Drawing.Color.DarkBlue;
            this.llInterface.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llInterface_LinkClicked);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.LimeGreen;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnStart.ForeColor = System.Drawing.Color.White;
            this.btnStart.Location = new System.Drawing.Point(12, 279);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(174, 47);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start Server";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tmrLog
            // 
            this.tmrLog.Enabled = true;
            this.tmrLog.Interval = 300;
            this.tmrLog.Tick += new System.EventHandler(this.tmrLog_Tick);
            // 
            // FMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(918, 404);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Coatl DNS";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMain_FormClosing);
            this.Shown += new System.EventHandler(this.FMain_Shown);
            this.Resize += new System.EventHandler(this.FMain_Resize);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.LinkLabel llInterface;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.LinkLabel llConfigCoatlDNS;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txLog;
        private System.Windows.Forms.NotifyIcon niTray;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.LinkLabel llClearLog;
        private System.Windows.Forms.ComboBox ddLogLevel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbCoatlDNS;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbEntries;
        private System.Windows.Forms.Timer tmrLog;
    }
}