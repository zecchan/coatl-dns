﻿using CoatlDNSCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoatlDNS
{
    public partial class FConfigClientFilter : Form
    {
        public CoatlDNSConfig AppConfig { get; set; }

        public FConfigClientFilter()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FConfigClientFilter_Load(object sender, EventArgs e)
        {

        }

        private void FConfigClientFilter_Shown(object sender, EventArgs e)
        {
            rbAllowAll.Checked = AppConfig.Server.ClientFilterMode == FilterMode.AllowAll;
            rbBlacklist.Checked = AppConfig.Server.ClientFilterMode == FilterMode.Blacklist;
            rbWhitelist.Checked = AppConfig.Server.ClientFilterMode == FilterMode.Whitelist;
        }

        private void rbAllowAll_CheckedChanged(object sender, EventArgs e)
        {
            if (rbAllowAll.Checked)
            {
                txEntries.Clear();
                txEntries.Enabled = false;
            }
        }

        private void rbBlacklist_CheckedChanged(object sender, EventArgs e)
        {
            if (rbBlacklist.Checked)
            {
                txEntries.Text = string.Join("\r\n", AppConfig.Server.ClientBlacklist.Select(i => i.ToString()));
                txEntries.Enabled = true;
            }
        }

        private void rbWhitelist_CheckedChanged(object sender, EventArgs e)
        {
            if (rbWhitelist.Checked)
            {
                txEntries.Text = string.Join("\r\n", AppConfig.Server.ClientWhitelist.Select(i => i.ToString()));
                txEntries.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AppConfig.Server.ClientFilterMode = rbWhitelist.Checked ? FilterMode.Whitelist : rbBlacklist.Checked ? FilterMode.Blacklist : FilterMode.AllowAll;
            if (rbWhitelist.Checked)
            {
                AppConfig.Server.ClientWhitelist.Clear();
                foreach(var line in txEntries.Lines)
                {
                    if (string.IsNullOrWhiteSpace(line)) continue;
                    try
                    {
                        var ip = IPAddress.Parse(line);
                        AppConfig.Server.ClientWhitelist.Add(ip);
                    }
                    catch { }
                }
            }
            if (rbBlacklist.Checked)
            {
                AppConfig.Server.ClientBlacklist.Clear();
                foreach (var line in txEntries.Lines)
                {
                    if (string.IsNullOrWhiteSpace(line)) continue;
                    try
                    {
                        var ip = IPAddress.Parse(line);
                        AppConfig.Server.ClientBlacklist.Add(ip);
                    }
                    catch { }
                }
            }
            AppConfig.Save();
            Close();
        }
    }
}
