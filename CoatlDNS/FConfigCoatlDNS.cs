﻿using ARSoft.Tools.Net;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoatlDNS
{
    public partial class FConfigCoatlDNS : Form
    {
        public CoatlDNSConfig Config { get; set; }

        public FConfigCoatlDNS()
        {
            InitializeComponent();
        }

        private void FConfigCoatlDNS_Load(object sender, EventArgs e)
        {
            lbWorkDir.Text = Environment.CurrentDirectory;
            lbArgs.Text = Program.Arguments;

            lbReadable.Text = "Yes";
            lbReadable.ForeColor = Color.Green;
            try
            {
                var file = Directory.EnumerateFiles(Environment.CurrentDirectory).FirstOrDefault();
                if (file != null)
                {
                    using (var fi = File.OpenRead(file))
                    {
                        if (fi.Length > 0)
                        {
                            var byt = fi.ReadByte();
                        }
                    }
                }
            }
            catch
            {
                lbReadable.Text = "No";
                lbReadable.ForeColor = Color.Red;
            }

            lbWriteable.Text = "Yes";
            lbWriteable.ForeColor = Color.Green;
            try
            {
                using (var f = File.CreateText("test"))
                {
                    f.WriteLine("TEST");
                }
                File.Delete("test");
            }
            catch
            {
                lbWriteable.Text = "No";
                lbWriteable.ForeColor = Color.Red;
            }
        }

        bool escapeCxStartup = false;
        private void FConfigCoatlDNS_Shown(object sender, EventArgs e)
        {
            cxMinTray.Checked = Config.MinimizeToTray;
            cxCache.Checked = Config.Server.UseCache;

            txServerName.Text = Config.Server.Name?.ToString() ?? "";
            try
            {
                nudCache.Value = Config.Server.MaximumCacheCount;
            }
            catch { nudCache.Value = 1000; }
            try
            {
                var skRun = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                try
                {
                    var val = skRun.GetValue("CoatlDNS", null)?.ToString();
                    if (val != null)
                    {
                        escapeCxStartup = true;
                        cxAutoStart.Checked = true;
                        escapeCxStartup = false;
                    }
                }
                finally
                {
                    skRun.Close();
                }
            }
            catch { }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DomainName servName = null;
            if (!string.IsNullOrWhiteSpace(txServerName.Text))
            {
                if (!DomainName.TryParse(txServerName.Text.Trim(), out servName))
                {
                    MessageBox.Show("Server name must be a valid domain or host name!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (saveAutoStart)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.UseShellExecute = true;
                startInfo.WorkingDirectory = Environment.CurrentDirectory;
                startInfo.FileName = Application.ExecutablePath;
                startInfo.Verb = "runas";
                startInfo.Arguments = cxAutoStart.Checked ? "-regstartup" : "-unregstartup";
                try
                {
                    Process p = Process.Start(startInfo);
                    p.WaitForExit();
                    if (p.ExitCode != 0)
                        throw new Exception();
                }
                catch
                {
                    MessageBox.Show("Failed to set windows startup configuration.\r\nChanges are not saved.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            Config.MinimizeToTray = cxMinTray.Checked;
            Config.Server.UseCache = cxCache.Checked;
            Config.Server.Name = servName;
            Config.Server.MaximumCacheCount = (int)Math.Round(nudCache.Value);
            Config.Save();
            Close();
        }

        bool saveAutoStart = false;
        private void cxAutoStart_CheckedChanged(object sender, EventArgs e)
        {
            if (escapeCxStartup) return;
            saveAutoStart = true;
            WinInterop.AddShieldToButton(button1);
        }

        private void cxCache_CheckedChanged(object sender, EventArgs e)
        {
            nudCache.Enabled = cxCache.Checked;
        }
    }
}
