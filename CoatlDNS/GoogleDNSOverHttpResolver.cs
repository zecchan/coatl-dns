﻿using ARSoft.Tools.Net;
using ARSoft.Tools.Net.Dns;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;

namespace CoatlDNS
{
    public class GoogleDNSOverHttpResolver
    {
        public DnsMessage Resolve(DomainName domainName, RecordType recordType, RecordClass recordClass)
        {
            if (domainName == null) return null;

            var url = "https://dns.google.com/resolve?";
            url += "name=" + domainName.ToString();
            url += "&type=" + recordType.ToString();
            url += "&class=" + recordClass.ToString();

            HttpWebRequest request = (HttpWebRequest)
                             WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var resp = request.GetResponse();
            try
            {
                var strm = resp.GetResponseStream();
                string json = null;
                using (var reader = new StreamReader(strm))
                {
                    json = reader.ReadToEnd();
                }
                if (json != null)
                {
                    dynamic jobj = JsonConvert.DeserializeObject(json);
                    DnsMessage msg = new DnsMessage();
                    // Read Status
                    try
                    {
                        msg.ReturnCode = (ReturnCode)((int)jobj.Status);
                    }
                    catch { }
                    // Read TC
                    try
                    {
                        msg.IsTruncated = ((bool)jobj.TC);
                    }
                    catch { }
                    // Read RD
                    try
                    {
                        msg.IsRecursionDesired = ((bool)jobj.RD);
                    }
                    catch { }
                    // Read RA
                    try
                    {
                        msg.IsRecursionAllowed = ((bool)jobj.RA);
                    }
                    catch { }
                    // Read AD
                    try
                    {
                        msg.IsAuthenticData = ((bool)jobj.AD);
                    }
                    catch { }
                    // Read CD
                    try
                    {
                        msg.IsCheckingDisabled = ((bool)jobj.CD);
                    }
                    catch { }
                    // Read Answers
                    try
                    {
                        foreach (var ans in jobj.Answer)
                        {
                            try
                            {
                                var rec = ReadRecords(ans);
                                if (rec != null)
                                {
                                    msg.AnswerRecords.Add(rec);
                                }
                            }
                            catch { }
                        }
                    }
                    catch { }
                    // Read Additional Records
                    try
                    {
                        foreach (var ans in jobj.Additional)
                        {
                            try
                            {
                                var rec = ReadRecords(ans);
                                if (rec != null)
                                {
                                    msg.AdditionalRecords.Add(rec);
                                }
                            }
                            catch { }
                        }
                    }
                    catch { }
                    // Read Authority Record
                    try
                    {
                        foreach (var ans in jobj.Authority)
                        {
                            try
                            {
                                var rec = ReadRecords(ans);
                                if (rec != null)
                                {
                                    msg.AuthorityRecords.Add(rec);
                                }
                            }
                            catch { }
                        }
                    }
                    catch { }

                    return msg;
                }
            }
            catch { }
            return null;
        }

        private DnsRecordBase ReadRecords(dynamic ans)
        {
            var typ = (RecordType)((int)ans.type);
            string name = null;
            try
            {
                name = ans.name;
            }
            catch { }
            int ttl = 0;
            try
            {
                ttl = ans.TTL;
            }
            catch { }
            string data = null;
            try
            {
                data = ans.data;
            }
            catch { }
            
            try
            {
                var srcDomain = DomainName.Parse(name);
                switch (typ)
                {
                    case RecordType.A:
                        return new ARecord(srcDomain, ttl, IPAddress.Parse(data));
                    case RecordType.Aaaa:
                        return new AaaaRecord(srcDomain, ttl, IPAddress.Parse(data));
                    case RecordType.CName:
                        return new CNameRecord(srcDomain, ttl, DomainName.Parse(data));
                    case RecordType.Ns:
                        return new NsRecord(srcDomain, ttl, DomainName.Parse(data));
                    case RecordType.Soa:
                        return ParseSoaRecord(srcDomain, ttl,data);
                    case RecordType.Mx:
                        return ParseMxRecord(srcDomain, ttl, data);
                    default:
                        return null;
                }
            }
            catch { return null; }
        }

        public MxRecord ParseMxRecord(DomainName domainName, int ttl, string data)
        {
            if (data == null) return null;
            var spl = data.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (spl.Length == 2)
            {
                return new MxRecord(domainName, ttl, ushort.Parse(spl[0]), DomainName.Parse(spl[1]));
            }
            return null;
        }

        public SoaRecord ParseSoaRecord(DomainName domainName, int ttl, string data)
        {
            if (data == null) return null;
            var spl = data.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (spl.Length == 7)
            {
                return new SoaRecord(domainName, ttl, DomainName.Parse(spl[0]), DomainName.Parse(spl[1]), uint.Parse(spl[2]), int.Parse(spl[3]), int.Parse(spl[4]), int.Parse(spl[5]), int.Parse(spl[6]));
            }
            return null;
        }
    }
}
