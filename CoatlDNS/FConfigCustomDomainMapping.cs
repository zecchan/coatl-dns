﻿using ARSoft.Tools.Net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace CoatlDNS
{
    public partial class FConfigCustomDomainMapping : Form
    {
        public CoatlDNSConfig AppConfig { get; set; }

        public FConfigCustomDomainMapping()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        Dictionary<DomainName, IPAddress[]> MappingState = new Dictionary<DomainName, IPAddress[]>();
        private void FConfigCustomDomainMapping_Shown(object sender, EventArgs e)
        {
            lbDomain.Items.Clear();
            txEntries.Clear();
            MappingState.Clear();
            try
            {
                nudTTL.Value = AppConfig.Server.CustomDomainMappingTTL;
            }
            catch { nudTTL.Value = 200; }
            foreach (var dm in AppConfig.Server.CustomDomainMapping)
            {
                MappingState.Add(DomainName.Parse(dm.Key.ToString()), dm.Value.Select(ip => IPAddress.Parse(ip.ToString())).ToArray());
                lbDomain.Items.Add(dm.Key.ToString());
            }
            if (lbDomain.Items.Count > 0)
                lbDomain.SelectedIndex = 0;
        }

        private void btnAddDomain_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txAddDomain.Text))
            {
                MessageBox.Show("Please enter a domain name!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                var dn = DomainName.Parse(txAddDomain.Text.Trim());
                if (MappingState.ContainsKey(dn))
                {
                    MessageBox.Show(dn.ToString() + " already exists!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                MappingState.Add(dn, new IPAddress[0]);
                lbDomain.Items.Add(dn.ToString());
                txAddDomain.Clear();
                txAddDomain.Focus();
            }
            catch
            {
                MessageBox.Show(txAddDomain.Text.Trim() + " is not a valid domain name!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txAddDomain_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                btnAddDomain.PerformClick();
            }
        }

        private void lbDomain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                e.Handled = true;
                if (lbDomain.SelectedIndex >= 0)
                {
                    var dn = MappingState.Keys.ElementAt(lbDomain.SelectedIndex);
                    if (dn.Equals(DomainName.Parse("dns.google.com")) || dn.Equals(DomainName.Parse("dns.google")))
                    {
                        MessageBox.Show("Cannot delete this entry, this entry is used by the system.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    MappingState.Remove(MappingState.Keys.ElementAt(lbDomain.SelectedIndex));
                    lbDomain.Items.RemoveAt(lbDomain.SelectedIndex);
                    if (lbDomain.Items.Count > 0)
                        lbDomain.SelectedIndex = 0;
                }
            }
        }

        DomainName _currentDomain = null;
        private void lbDomain_SelectedIndexChanged(object sender, EventArgs e)
        {
            var _lastDomain = _currentDomain;
            if (lbDomain.SelectedIndex < 0)
                _currentDomain = null;
            else
                _currentDomain = MappingState.Keys.ElementAt(lbDomain.SelectedIndex);
            if (_lastDomain != null && MappingState.ContainsKey(_lastDomain))
            {
                List<IPAddress> adrs = new List<IPAddress>();
                foreach(var line in txEntries.Lines)
                {
                    if (string.IsNullOrWhiteSpace(line)) continue;
                    var ln = line.Trim();
                    try
                    {
                        var ip = IPAddress.Parse(ln);
                        adrs.Add(ip);
                    }
                    catch { }
                }

                if (!(_lastDomain.Equals(DomainName.Parse("dns.google.com")) || _lastDomain.Equals(DomainName.Parse("dns.google"))) || adrs.Count > 0)
                {
                    MappingState[_lastDomain] = adrs.ToArray();
                }
            }
            txEntries.Clear();
            if (_currentDomain != null && MappingState.ContainsKey(_currentDomain))
            {
                var ips = MappingState[_currentDomain];
                txEntries.Text = string.Join("\r\n", ips.Select(ip => ip.ToString()));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lbDomain_SelectedIndexChanged(lbDomain, e);
            AppConfig.Server.CustomDomainMapping.Clear();
            foreach(var k in MappingState)
            {
                AppConfig.Server.CustomDomainMapping.Add(k.Key, k.Value);
            }
            AppConfig.Server.CustomDomainMappingTTL = (int)Math.Round(nudTTL.Value);
            AppConfig.Save();
            Close();
        }
    }
}
