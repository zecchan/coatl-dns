﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoatlDNS
{
    public partial class FSplashScreen : Form
    {
        public FSplashScreen()
        {
            InitializeComponent();
        }

        private void tStartMain_Tick(object sender, EventArgs e)
        {
            tStartMain.Stop();
            Close();
        }
    }
}
