﻿using ARSoft.Tools.Net;
using CoatlDNSCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoatlDNS
{
    public partial class FConfigDomainFilter : Form
    {
        public CoatlDNSConfig AppConfig { get; set; }

        public FConfigDomainFilter()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FConfigDomainFilter_Shown(object sender, EventArgs e)
        {
            rbAllowAll.Checked = AppConfig.Server.DomainFilterMode == FilterMode.AllowAll;
            rbBlacklist.Checked = AppConfig.Server.DomainFilterMode == FilterMode.Blacklist;
            rbWhitelist.Checked = AppConfig.Server.DomainFilterMode == FilterMode.Whitelist;
        }

        private void rbAllowAll_CheckedChanged(object sender, EventArgs e)
        {
            if (rbAllowAll.Checked)
            {
                txEntries.Clear();
                txEntries.Enabled = false;
            }
        }

        private void rbBlacklist_CheckedChanged(object sender, EventArgs e)
        {
            if (rbBlacklist.Checked)
            {
                txEntries.Text = string.Join("\r\n", AppConfig.Server.DomainBlacklist.Select(i => i.ToString()));
                txEntries.Enabled = true;
            }
        }

        private void rbWhitelist_CheckedChanged(object sender, EventArgs e)
        {
            if (rbWhitelist.Checked)
            {
                txEntries.Text = string.Join("\r\n", AppConfig.Server.DomainWhitelist.Select(i => i.ToString()));
                txEntries.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AppConfig.Server.DomainFilterMode = rbWhitelist.Checked ? FilterMode.Whitelist : rbBlacklist.Checked ? FilterMode.Blacklist : FilterMode.AllowAll;
            if (rbWhitelist.Checked)
            {
                AppConfig.Server.DomainWhitelist.Clear();
                foreach (var line in txEntries.Lines)
                {
                    if (string.IsNullOrWhiteSpace(line)) continue;
                    try
                    {
                        var dn = DomainName.Parse(line);
                        AppConfig.Server.DomainWhitelist.Add(dn);
                    }
                    catch { }
                }
            }
            if (rbBlacklist.Checked)
            {
                AppConfig.Server.DomainBlacklist.Clear();
                foreach (var line in txEntries.Lines)
                {
                    if (string.IsNullOrWhiteSpace(line)) continue;
                    try
                    {
                        var dn = DomainName.Parse(line);
                        AppConfig.Server.DomainBlacklist.Add(dn);
                    }
                    catch { }
                }
            }
            AppConfig.Save();
            Close();
        }
    }
}
