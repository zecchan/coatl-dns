﻿using ARSoft.Tools.Net;
using ARSoft.Tools.Net.Dns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoatlDNSCore
{
    public class CoatlDNSServer
    {
        public static Version Version { get; } = new Version(1, 4, 3);

        /// <summary>
        /// Gets or sets the DNS Server Name
        /// </summary>
        public DomainName Name { get; set; } = null;

        /// <summary>
        /// Gets the underlying DNSServer by ARSoft
        /// </summary>
        public List<DnsServer> Servers { get; } = new List<DnsServer>();

        public List<IPEndPoint> BoundEndPoints { get => BoundAddresses.ToArray().ToList(); }
        private List<IPEndPoint> BoundAddresses { get; } = new List<IPEndPoint>();

        /// <summary>
        /// Gets or sets the timeout for sending and receiving data
        /// </summary>
        public int Timeout { get => Servers.FirstOrDefault()?.Timeout ?? -1; set { foreach (var s in Servers) if (s != null) s.Timeout = value; } }

        /// <summary>
        /// Creates a new Coatl managed DNSServer
        /// </summary>
        /// <param name="endPoint">The endpoint to bind the listener to</param>
        /// <param name="listenerCount">How many listener thread that will be created</param>
        public CoatlDNSServer(IPEndPoint endPoint, int listenerCount = 30)
        {

            var Server = new DnsServer(endPoint, listenerCount, listenerCount);
            Server.ClientConnected += Server_ClientConnected;
            Server.QueryReceived += Server_QueryReceived;
            Servers.Add(Server);
            BoundAddresses.Add(endPoint);
        }

        /// <summary>
        /// Creates many new Coatl managed DNSServers
        /// </summary>
        /// <param name="endPoints">The endpoint to bind the listener to</param>
        /// <param name="listenerCount">How many listener thread that will be created</param>
        public CoatlDNSServer(IPEndPoint[] endPoints, int listenerCount = 30)
        {
            foreach (var endPoint in endPoints)
            {
                var Server = new DnsServer(endPoint, listenerCount, listenerCount);
                Server.ClientConnected += Server_ClientConnected;
                Server.QueryReceived += Server_QueryReceived;
                Servers.Add(Server);
                BoundAddresses.Add(endPoint);
            }
        }
        /// <summary>
        /// Creates a new Coatl managed DNSServer
        /// </summary>
        /// <param name="ipAddress">The endpoint to bind the listener to</param>
        /// <param name="listenerCount">How many listener thread that will be created</param>
        public CoatlDNSServer(IPAddress ipAddress, int listenerCount = 30)
        {

            var Server = new DnsServer(ipAddress, listenerCount, listenerCount);
            Server.ClientConnected += Server_ClientConnected;
            Server.QueryReceived += Server_QueryReceived;
            Servers.Add(Server);
            BoundAddresses.Add(new IPEndPoint(ipAddress, 53));
        }

        /// <summary>
        /// Creates many new Coatl managed DNSServers
        /// </summary>
        /// <param name="ipAddresses">The endpoint to bind the listener to</param>
        /// <param name="listenerCount">How many listener thread that will be created</param>
        public CoatlDNSServer(IPAddress[] ipAddresses, int listenerCount = 30)
        {
            foreach (var ipAddress in ipAddresses)
            {
                var Server = new DnsServer(new IPEndPoint(ipAddress, 53), listenerCount, listenerCount);
                Server.ClientConnected += Server_ClientConnected;
                Server.QueryReceived += Server_QueryReceived;
                Servers.Add(Server);
                BoundAddresses.Add(new IPEndPoint(ipAddress, 53));
            }
        }

        /// <summary>
        /// Gets whether the server is currently running or not
        /// </summary>
        public bool IsRunning { get; private set; }

        /// <summary>
        /// Starts the server
        /// </summary>
        public void Start()
        {
            if (IsRunning) return;
            var started = new List<DnsServer>();
            foreach (var s in Servers)
                try
                {
                    s.Start();
                    started.Add(s);
                }
                catch(Exception ex)
                {
                    foreach (var sv in started)
                        try
                        {
                            sv.Stop();
                        }
                        catch { }
                    throw ex;
                }
            IsRunning = true;
        }

        /// <summary>
        /// Stops the server
        /// </summary>
        public void Stop()
        {
            if (!IsRunning) return;
            foreach (var s in Servers)
                try
                {
                    s.Stop();
                }
                catch { }
            IsRunning = false;
        }

        /// <summary>
        /// Gets or sets filter mode for connected client
        /// </summary>
        public FilterMode ClientFilterMode { get; set; } = FilterMode.AllowAll;

        /// <summary>
        /// Gets a list of blacklisted client IP Addresses
        /// </summary>
        public List<IPAddress> ClientBlacklist { get; } = new List<IPAddress>();

        /// <summary>
        /// Gets a list of whitelisted client IP Addresses
        /// </summary>
        public List<IPAddress> ClientWhitelist { get; } = new List<IPAddress>();

        /// <summary>
        /// Gets or sets filter mode for requested domain
        /// </summary>
        public FilterMode DomainFilterMode { get; set; } = FilterMode.AllowAll;

        /// <summary>
        /// Gets a list of blacklisted domain name
        /// </summary>
        public List<DomainName> DomainBlacklist { get; } = new List<DomainName>();

        /// <summary>
        /// Gets a list of whitelisted domain name
        /// </summary>
        public List<DomainName> DomainWhitelist { get; } = new List<DomainName>();

        /// <summary>
        /// Gets a list of custom domain mapping that allows you to provide custom IP for a given domain
        /// </summary>
        public Dictionary<DomainName, IPAddress[]> CustomDomainMapping { get; } = new Dictionary<DomainName, IPAddress[]>();

        /// <summary>
        /// How long should the dns record for custom domain mapping will be cached (in seconds)
        /// </summary>
        public int CustomDomainMappingTTL { get; set; } = 200;

        #region Caching
        /// <summary>
        /// Gets cached DNS Messages from resolver
        /// </summary>
        public Dictionary<DNSRecordCacheKey, DnsMessage> DNSCache { get; } = new Dictionary<DNSRecordCacheKey, DnsMessage>();

        /// <summary>
        /// Gets or sets whether the server will cache DNS Messages sent by the resolver
        /// </summary>
        public bool UseCache { get; set; } = true;

        /// <summary>
        /// Gets or sets the maximum cached DNS Message count the server will record
        /// </summary>
        public int MaximumCacheCount { get => _maxCache; set => _maxCache = Math.Max(1, value); }
        int _maxCache = 1000;

        /// <summary>
        /// Clears the server cache
        /// </summary>
        public void ClearCache()
        {
            DNSCache.Clear();
        }
        #endregion

        private async Task Server_ClientConnected(object sender, ClientConnectedEventArgs e)
        {
            if (ClientFilterMode == FilterMode.Blacklist)
            {
                if (ClientBlacklist.Where(c => c.Equals(e.RemoteEndpoint.Address)).Count() > 0)
                {
                    e.RefuseConnect = true;
                    await Task.Run(() => OnClientConnectionBlocked?.Invoke(this, e.RemoteEndpoint, "blacklisted"));
                    return;
                }
            }

            if (ClientFilterMode == FilterMode.Whitelist)
            {
                if (ClientWhitelist.Where(c => c.Equals(e.RemoteEndpoint.Address)).Count() == 0)
                {
                    e.RefuseConnect = true;
                    await Task.Run(() => OnClientConnectionBlocked?.Invoke(this, e.RemoteEndpoint, "not in whitelist"));
                    return;
                }
            }

            await Task.Run(() => OnClientConnected?.Invoke(this, e.RemoteEndpoint));
        }

        private async Task Server_QueryReceived(object sender, QueryReceivedEventArgs e)
        {
            var query = e.Query as DnsMessage;

            if (query == null) return;

            var response = query.CreateResponseInstance();
            DnsMessage message = null;

            response.ReturnCode = ReturnCode.FormatError;
            if (response.Questions.Count > 0)
            {
                var question = response.Questions[0];
                var cacheKey = new DNSRecordCacheKey(question.Name, question.RecordType, question.RecordClass);
                OnQueryReceived?.Invoke(this, e.RemoteEndpoint, question.Name, question.RecordType, question.RecordClass);
                response.ReturnCode = ReturnCode.NoError;

                if (CustomDomainMapping.ContainsKey(question.Name) && CustomDomainMapping[question.Name] != null
                    && CustomDomainMapping[question.Name].Length > 0
                    && (question.RecordType == RecordType.A || question.RecordType == RecordType.Aaaa))
                {
                    message = new DnsMessage();
                    var addrs = CustomDomainMapping[question.Name];
                    if (question.RecordType == RecordType.A)
                    {
                        foreach (var addr in addrs.Where(a => a.AddressFamily == AddressFamily.InterNetwork))
                            message.AnswerRecords.Add(new ARecord(question.Name, CustomDomainMappingTTL, addr));
                    }
                    if (question.RecordType == RecordType.Aaaa)
                    {
                        foreach (var addr in addrs.Where(a => a.AddressFamily == AddressFamily.InterNetworkV6))
                            message.AnswerRecords.Add(new AaaaRecord(question.Name, CustomDomainMappingTTL, addr));
                    }
                    message.IsTruncated = false;
                    message.IsRecursionDesired = true;
                    message.IsRecursionAllowed = true;
                    message.IsAuthenticData = false;
                    message.IsCheckingDisabled = false;
                    message.IsAuthoritiveAnswer = false;
                    message.ReturnCode = ReturnCode.NoError;
                }
                else if (UseCache && DNSCache.ContainsKey(cacheKey))
                {
                    message = DNSCache[cacheKey];
                }
                else if (question.RecordType == RecordType.Ptr)
                {
                    message = await Task.Run(() => ReverseResolve(question.Name, question.RecordType, question.RecordClass));
                }
                else
                {
                    if (DomainFilterMode == FilterMode.Blacklist)
                    {
                        if (DomainBlacklist.Where(d => d.IsEqualOrSubDomainOf(question.Name)).Count() > 0)
                        {
                            response.ReturnCode = ReturnCode.NxDomain;
                            OnDomainBlacklisted?.Invoke(this, e.RemoteEndpoint, question.Name, question.RecordType, question.RecordClass);
                            return;
                        }
                    }
                    if (DomainFilterMode == FilterMode.Whitelist)
                    {
                        if (DomainWhitelist.Where(d => d.IsEqualOrSubDomainOf(question.Name)).Count() == 0)
                        {
                            response.ReturnCode = ReturnCode.NxDomain;
                            OnDomainNotWhitelisted?.Invoke(this, e.RemoteEndpoint, question.Name, question.RecordType, question.RecordClass);
                            return;
                        }
                    }

                    message = await Task.Run(() => OnResolveRequest?.Invoke(this, question.Name, question.RecordType, question.RecordClass));
                }

                if (message != null)
                {
                    response.AnswerRecords.Clear();
                    response.AnswerRecords.AddRange(message.AnswerRecords);
                    response.AdditionalRecords.Clear();
                    response.AdditionalRecords.AddRange(message.AdditionalRecords);
                    response.AuthorityRecords.Clear();
                    response.AuthorityRecords.AddRange(message.AuthorityRecords);
                    response.IsAuthenticData = message.IsAuthenticData;
                    response.IsCheckingDisabled = message.IsCheckingDisabled;
                    response.IsTruncated = message.IsTruncated;
                    response.IsRecursionDesired = message.IsRecursionDesired;
                    response.IsRecursionAllowed = message.IsRecursionAllowed;
                    response.ReturnCode = message.ReturnCode;

                    if (UseCache && response.ReturnCode == ReturnCode.NoError)
                    {
                        while (DNSCache.Count >= MaximumCacheCount)
                        {
                            DNSCache.Remove(DNSCache.Keys.ElementAt(0));
                        }
                        if (DNSCache.ContainsKey(cacheKey))
                            DNSCache[cacheKey] = message;
                        else
                            DNSCache.Add(cacheKey, message);
                    }

                    e.Response = response;

                    OnAnswerSent?.Invoke(this, e.RemoteEndpoint, question.Name, question.RecordType, question.RecordClass);
                }
                else
                {
                    response.ReturnCode = ReturnCode.NotZone;
                    OnQueryFailed?.Invoke(this, e.RemoteEndpoint, question.Name, question.RecordType, question.RecordClass);
                }
            }
        }

        public async Task<DnsMessage> ReverseResolve(DomainName domainName, RecordType recordType, RecordClass recordClass)
        {
            IPAddress addr = null;
            // get ip
            var lbl = domainName.Labels.ToArray();
            Array.Reverse(lbl);
            var lb = new List<string>();
            for (var i = 2; i < lbl.Length; i++) lb.Add(lbl[i]);
            try
            {
                var ipver = lbl[1];
                var ipstr = "";
                if (ipver == "in-addr")
                {
                    ipstr = string.Join(".", lb);
                }
                if (ipver == "ip6")
                {
                    ipstr = string.Join(":", lb.Select(l =>
                    {
                        if (l.StartsWith("000") && l.Length > 3) return l.Substring(3);
                        if (l.StartsWith("00") && l.Length > 2) return l.Substring(2);
                        if (l.StartsWith("0") && l.Length > 1) return l.Substring(1);
                        return l;
                    }));
                    while (ipstr.IndexOf(":0:") >= 0)
                        ipstr = ipstr.Replace(":0:", "::");
                    while (ipstr.IndexOf(":::") >= 0)
                        ipstr = ipstr.Replace(":::", "::");
                    if (ipstr.StartsWith("0"))
                        ipstr = ipstr.Substring(1);
                }
                addr = IPAddress.Parse(ipstr);
            }
            catch
            {
            }
            if (addr == null) return null;

            DnsMessage message = null;
            if (!IsOwnIP(addr) && BoundAddresses.Where(a => a.ToString() == addr.ToString()).Count() <= 0)
            {
                message = await Task.Run(() => OnResolveRequest?.Invoke(this, domainName, recordType, recordClass));
            }
            else
            {
                message = new DnsMessage();
                message.IsTruncated = false;
                message.IsRecursionDesired = true;
                message.IsRecursionAllowed = true;
                message.IsAuthenticData = false;
                message.IsCheckingDisabled = false;
                message.IsAuthoritiveAnswer = true;
                message.ReturnCode = ReturnCode.NoError;

                try
                {
                    if (Name != null)
                        message.AnswerRecords.Add(new PtrRecord(domainName, CustomDomainMappingTTL, Name));
                    else
                        message.AnswerRecords.Add(new PtrRecord(domainName, CustomDomainMappingTTL, DomainName.Parse(Environment.MachineName)));
                }
                catch
                {
                    message.AnswerRecords.Add(new PtrRecord(domainName, CustomDomainMappingTTL, DomainName.Parse("CoatlDNS")));
                }
            }

            return message;
        }

        public bool IsOwnIP(IPAddress address)
        {
            var ipstr = address.ToString();
            var lv4 = IPAddress.Loopback.ToString();
            var lv6 = IPAddress.IPv6Loopback.ToString();
            return ipstr == lv4 || ipstr == lv6;
        }

        public event DNSResolveRequestEvent OnResolveRequest;
        public event DNSQueryEvent OnAnswerSent;
        public event DNSQueryEvent OnQueryFailed;
        public event DNSQueryEvent OnDomainBlacklisted;
        public event DNSQueryEvent OnDomainNotWhitelisted;
        public event ClientConnectedEvent OnClientConnected;
        public event ClientBlockedEvent OnClientConnectionBlocked;
        public event DNSQueryEvent OnQueryReceived;
    }

    public delegate DnsMessage DNSResolveRequestEvent(object sender, DomainName domainName, RecordType recordType, RecordClass recordClass);
    public delegate void DNSQueryEvent(object sender, IPEndPoint remoteEndPoint, DomainName domainName, RecordType recordType, RecordClass recordClass);
    public delegate void ClientConnectedEvent(object sender, IPEndPoint remoteEndPoint);
    public delegate void ClientBlockedEvent(object sender, IPEndPoint remoteEndPoint, string blockReason = null);

    public enum FilterMode
    {
        AllowAll = 0,

        Whitelist = 1,
        Blacklist = 2,
    }

    public class FullyQualifiedDomainName
    {
        public FullyQualifiedDomainName(string address)
        {
            string pattern = @"^([a-zA-Z0-9]+\.)+([a-zA-Z0-9]+)\.?$";
            if (address == null)
                throw new ArgumentNullException("address");
            if (!Regex.IsMatch(address.Trim(), pattern))
                throw new Exception("Invalid domain name: " + address);
            var spl = address.Trim().Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            TopLevelDomain = spl[spl.Length - 1];
            Parts = spl;
            Address = address.Trim();
            if (!Address.EndsWith(".")) Address += ".";
        }

        /// <summary>
        /// Gets the top level domain of the address
        /// </summary>
        public string TopLevelDomain { get; }

        /// <summary>
        /// Gets the address
        /// </summary>
        public string Address { get; }

        /// <summary>
        /// Gets the parts of the domain name
        /// </summary>
        public string[] Parts { get; }

        /// <summary>
        /// Gets the invert of the domain name
        /// </summary>
        public FullyQualifiedDomainName Invert()
        {
            var p = Parts.Reverse().ToArray();
            var addr = string.Join(".", p);
            return new FullyQualifiedDomainName(addr);
        }

        /// <summary>
        /// Checks whether this fqdns is a subdomain of specified fqdn
        /// </summary>
        /// <param name="fqdn">The supposedly parent domain</param>
        public bool IsSubdomainOf(FullyQualifiedDomainName fqdn)
        {
            if (fqdn == null) return false;
            if (fqdn.Parts.Length < Parts.Length) return false;
            var istart = fqdn.Parts.Length - Parts.Length;
            for (var i = istart; i < fqdn.Parts.Length; i++)
            {
                if (fqdn.Parts[i].ToLower() != Parts[i - istart].ToLower())
                    return false;
            }
            return true;
        }

        public static bool operator ==(FullyQualifiedDomainName a, FullyQualifiedDomainName b) => a.Address.ToLower() == b.Address.ToLower();

        public static bool operator !=(FullyQualifiedDomainName a, FullyQualifiedDomainName b) => a.Address.ToLower() != b.Address.ToLower();

        public static implicit operator FullyQualifiedDomainName(string address)
        {
            return new FullyQualifiedDomainName(address);
        }

        public override bool Equals(object obj)
        {
            if (obj is FullyQualifiedDomainName)
            {
                return this == (FullyQualifiedDomainName)obj;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return Address;
        }
    }

    public struct DNSRecordCacheKey : IEquatable<DNSRecordCacheKey>
    {
        DomainName DomainName { get; }
        RecordType RecordType { get; }
        RecordClass RecordClass { get; }

        public DNSRecordCacheKey(DomainName domainName, RecordType recordType, RecordClass recordClass)
        {
            DomainName = domainName;
            RecordType = recordType;
            RecordClass = recordClass;
        }

        public static bool operator ==(DNSRecordCacheKey a, DNSRecordCacheKey b) => a.Equals(b);
        public static bool operator !=(DNSRecordCacheKey a, DNSRecordCacheKey b) => !a.Equals(b);

        public override bool Equals(object obj)
        {
            if (obj is DNSRecordCacheKey)
            {
                var rck = (DNSRecordCacheKey)obj;
                return DomainName.Equals(rck.DomainName) && RecordType == rck.RecordType && RecordClass == rck.RecordClass;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool Equals(DNSRecordCacheKey other)
        {
            return Equals((object)other);
        }
    }
}
