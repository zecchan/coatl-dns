﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoatlDNSCore
{
    public class Logs
    {
        /// <summary>
        /// Gets recorded log data
        /// </summary>
        public Queue<LogData> Data { get; } = new Queue<LogData>();

        /// <summary>
        /// Gets or sets the maximum log data to be recorded
        /// </summary>
        public int MaxLogData
        {
            get
            {
                return _maxLogData;
            }
            set
            {
                _maxLogData = Math.Max(value, 1);
            }
        }
        int _maxLogData = 1000;
        
        /// <summary>
        /// Adds a new log data
        /// </summary>
        public void Add(string message, LogLevel level = LogLevel.Debug)
        {
            var ld = new LogData(message, level);
            while (Data.Count >= MaxLogData) Data.Dequeue();
            Data.Enqueue(ld);
            OnLogAdded?.Invoke(this, ld);
        }

        /// <summary>
        /// Adds a new log data
        /// </summary>
        public void Add(string message, DateTime timestamp, LogLevel level = LogLevel.Debug)
        {
            var ld = new LogData(message, timestamp, level);
            while (Data.Count >= MaxLogData) Data.Dequeue();
            Data.Enqueue(ld);
            OnLogAdded?.Invoke(this, ld);
        }

        /// <summary>
        /// Clears all log data
        /// </summary>
        public void Clear()
        {
            Data.Clear();
        }

        /// <summary>
        /// Compiles the log data into a string array
        /// </summary>
        /// <param name="minLogLevel">The minimum log level of outputted data</param>
        public IEnumerable<string> Compile(LogLevel minLogLevel = LogLevel.Debug)
        {
            return Data.Where(d => d.Level >= minLogLevel).Select(d => d.ToString());
        }

        /// <summary>
        /// Compiles the log data into a string
        /// </summary>
        /// <param name="minLogLevel">The minimum log level of outputted data</param>
        public string CompileString(LogLevel minLogLevel = LogLevel.Debug)
        {
            return string.Join("\r\n", Data.Where(d => d.Level >= minLogLevel).Select(d => d.ToString()));
        }

        /// <summary>
        /// An event that will trigger when a log is added
        /// </summary>
        public event EventHandler<LogData> OnLogAdded;

        public override string ToString()
        {
            return string.Join("\r\n", Data);
        }
    }

    public struct LogData
    {
        /// <summary>
        /// Gets the timestamp of the log
        /// </summary>
        public DateTime Timestamp { get; }

        /// <summary>
        /// Gets the message of the log
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// Gets the importance level of the log
        /// </summary>
        public LogLevel Level { get; }

        public override string ToString()
        {
            return $"[{Level} - {Timestamp.ToString("yyyy-MM-dd HH:mm:ss")}] {Message}";
        }

        public LogData(string message, LogLevel level = LogLevel.Debug)
        {
            Message = message;
            Level = level;
            Timestamp = DateTime.Now;
        }
        public LogData(string message, DateTime timestamp, LogLevel level = LogLevel.Debug)
        {
            Message = message;
            Level = level;
            Timestamp = timestamp;
        }
    }

    public enum LogLevel
    {
        /// <summary>
        /// Indicates that the log is used to write every action
        /// </summary>
        Verbose,
        /// <summary>
        /// Indicates that the log is used to write debug information
        /// </summary>
        Debug,
        /// <summary>
        /// Indicates that the log is used to notice an event or possible error
        /// </summary>
        Notice,
        /// <summary>
        /// Indicates that the log is used to warn about possible error
        /// </summary>
        Warning,
        /// <summary>
        /// Indicates that the log is used to inform that an error has happened
        /// </summary>
        Error,
        /// <summary>
        /// Indicates that the log is used to inform that an error has happened and the program will close
        /// </summary>
        FatalError
    }
}
